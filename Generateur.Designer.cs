﻿namespace Liste_Epiceri
{
    partial class Generateur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.labelSouper = new System.Windows.Forms.Label();
            this.labelVendredi = new System.Windows.Forms.Label();
            this.labelSamedi = new System.Windows.Forms.Label();
            this.labelDimanche = new System.Windows.Forms.Label();
            this.labelLundi = new System.Windows.Forms.Label();
            this.labelMardi = new System.Windows.Forms.Label();
            this.labelMercredi = new System.Windows.Forms.Label();
            this.labelJeudi = new System.Windows.Forms.Label();
            this.labelDiner = new System.Windows.Forms.Label();
            this.textBoxSouperVendredi = new System.Windows.Forms.TextBox();
            this.textBoxSouperSamedi = new System.Windows.Forms.TextBox();
            this.textBoxSouperDimanche = new System.Windows.Forms.TextBox();
            this.textBoxSouperLundi = new System.Windows.Forms.TextBox();
            this.textBoxSouperMardi = new System.Windows.Forms.TextBox();
            this.textBoxSouperMercredi = new System.Windows.Forms.TextBox();
            this.textBoxSouperJeudi = new System.Windows.Forms.TextBox();
            this.buttonSouperVendredi = new System.Windows.Forms.Button();
            this.buttonSouperSamedi = new System.Windows.Forms.Button();
            this.buttonSouperDimanche = new System.Windows.Forms.Button();
            this.buttonSouperLundi = new System.Windows.Forms.Button();
            this.buttonSouperMardi = new System.Windows.Forms.Button();
            this.buttonSouperMercredi = new System.Windows.Forms.Button();
            this.buttonSouperJeudi = new System.Windows.Forms.Button();
            this.buttonSouperAll = new System.Windows.Forms.Button();
            this.textBoxDinerSamedi = new System.Windows.Forms.TextBox();
            this.textBoxDinerDimanche = new System.Windows.Forms.TextBox();
            this.buttonDinerSamedi = new System.Windows.Forms.Button();
            this.buttonWeekAll = new System.Windows.Forms.Button();
            this.labelListe = new System.Windows.Forms.Label();
            this.textBoxListeFruits = new System.Windows.Forms.RichTextBox();
            this.buttonOutExcel = new System.Windows.Forms.Button();
            this.textBoxListeViande = new System.Windows.Forms.RichTextBox();
            this.textBoxListeBoulangerie = new System.Windows.Forms.RichTextBox();
            this.textBoxListeDejeuner = new System.Windows.Forms.RichTextBox();
            this.textBoxListeLait = new System.Windows.Forms.RichTextBox();
            this.textBoxListePate = new System.Windows.Forms.RichTextBox();
            this.textBoxListeGateau = new System.Windows.Forms.RichTextBox();
            this.textBoxListeCongelo = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxSouperVendredi = new System.Windows.Forms.ComboBox();
            this.comboBoxSouperSamedi = new System.Windows.Forms.ComboBox();
            this.comboBoxSouperDimanche = new System.Windows.Forms.ComboBox();
            this.comboBoxSouperLundi = new System.Windows.Forms.ComboBox();
            this.comboBoxSouperMardi = new System.Windows.Forms.ComboBox();
            this.comboBoxSouperMercredi = new System.Windows.Forms.ComboBox();
            this.comboBoxSouperJeudi = new System.Windows.Forms.ComboBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.buttonDinerDimanche = new System.Windows.Forms.Button();
            this.buttonDinerAll = new System.Windows.Forms.Button();
            this.checkBoxVendredi = new System.Windows.Forms.CheckBox();
            this.checkBoxSamedi = new System.Windows.Forms.CheckBox();
            this.checkBoxDimanche = new System.Windows.Forms.CheckBox();
            this.checkBoxLundi = new System.Windows.Forms.CheckBox();
            this.checkBoxMardi = new System.Windows.Forms.CheckBox();
            this.checkBoxMercredi = new System.Windows.Forms.CheckBox();
            this.checkBoxJeudi = new System.Windows.Forms.CheckBox();
            this.checkBoxDinerSamedi = new System.Windows.Forms.CheckBox();
            this.checkBoxDinerDimanche = new System.Windows.Forms.CheckBox();
            this.SelectSouper = new System.Windows.Forms.Button();
            this.SelectDiner = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(46, 23);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 0;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // labelSouper
            // 
            this.labelSouper.AutoSize = true;
            this.labelSouper.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.labelSouper.Location = new System.Drawing.Point(214, 63);
            this.labelSouper.Name = "labelSouper";
            this.labelSouper.Size = new System.Drawing.Size(136, 37);
            this.labelSouper.TabIndex = 1;
            this.labelSouper.Text = "Soupers";
            // 
            // labelVendredi
            // 
            this.labelVendredi.AutoSize = true;
            this.labelVendredi.Location = new System.Drawing.Point(43, 120);
            this.labelVendredi.Name = "labelVendredi";
            this.labelVendredi.Size = new System.Drawing.Size(49, 13);
            this.labelVendredi.TabIndex = 2;
            this.labelVendredi.Text = "Vendredi";
            // 
            // labelSamedi
            // 
            this.labelSamedi.AutoSize = true;
            this.labelSamedi.Location = new System.Drawing.Point(43, 166);
            this.labelSamedi.Name = "labelSamedi";
            this.labelSamedi.Size = new System.Drawing.Size(42, 13);
            this.labelSamedi.TabIndex = 3;
            this.labelSamedi.Text = "Samedi";
            // 
            // labelDimanche
            // 
            this.labelDimanche.AutoSize = true;
            this.labelDimanche.Location = new System.Drawing.Point(43, 209);
            this.labelDimanche.Name = "labelDimanche";
            this.labelDimanche.Size = new System.Drawing.Size(55, 13);
            this.labelDimanche.TabIndex = 4;
            this.labelDimanche.Text = "Dimanche";
            // 
            // labelLundi
            // 
            this.labelLundi.AutoSize = true;
            this.labelLundi.Location = new System.Drawing.Point(43, 256);
            this.labelLundi.Name = "labelLundi";
            this.labelLundi.Size = new System.Drawing.Size(33, 13);
            this.labelLundi.TabIndex = 5;
            this.labelLundi.Text = "Lundi";
            // 
            // labelMardi
            // 
            this.labelMardi.AutoSize = true;
            this.labelMardi.Location = new System.Drawing.Point(43, 301);
            this.labelMardi.Name = "labelMardi";
            this.labelMardi.Size = new System.Drawing.Size(33, 13);
            this.labelMardi.TabIndex = 6;
            this.labelMardi.Text = "Mardi";
            // 
            // labelMercredi
            // 
            this.labelMercredi.AutoSize = true;
            this.labelMercredi.Location = new System.Drawing.Point(44, 346);
            this.labelMercredi.Name = "labelMercredi";
            this.labelMercredi.Size = new System.Drawing.Size(48, 13);
            this.labelMercredi.TabIndex = 7;
            this.labelMercredi.Text = "Mercredi";
            // 
            // labelJeudi
            // 
            this.labelJeudi.AutoSize = true;
            this.labelJeudi.Location = new System.Drawing.Point(44, 396);
            this.labelJeudi.Name = "labelJeudi";
            this.labelJeudi.Size = new System.Drawing.Size(32, 13);
            this.labelJeudi.TabIndex = 8;
            this.labelJeudi.Text = "Jeudi";
            this.labelJeudi.Click += new System.EventHandler(this.labelJeudi_Click);
            // 
            // labelDiner
            // 
            this.labelDiner.AutoSize = true;
            this.labelDiner.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.labelDiner.Location = new System.Drawing.Point(641, 63);
            this.labelDiner.Name = "labelDiner";
            this.labelDiner.Size = new System.Drawing.Size(109, 37);
            this.labelDiner.TabIndex = 9;
            this.labelDiner.Text = "Dîners";
            // 
            // textBoxSouperVendredi
            // 
            this.textBoxSouperVendredi.Location = new System.Drawing.Point(119, 117);
            this.textBoxSouperVendredi.Name = "textBoxSouperVendredi";
            this.textBoxSouperVendredi.Size = new System.Drawing.Size(138, 20);
            this.textBoxSouperVendredi.TabIndex = 10;
            // 
            // textBoxSouperSamedi
            // 
            this.textBoxSouperSamedi.Location = new System.Drawing.Point(119, 163);
            this.textBoxSouperSamedi.Name = "textBoxSouperSamedi";
            this.textBoxSouperSamedi.Size = new System.Drawing.Size(138, 20);
            this.textBoxSouperSamedi.TabIndex = 11;
            // 
            // textBoxSouperDimanche
            // 
            this.textBoxSouperDimanche.Location = new System.Drawing.Point(119, 206);
            this.textBoxSouperDimanche.Name = "textBoxSouperDimanche";
            this.textBoxSouperDimanche.Size = new System.Drawing.Size(138, 20);
            this.textBoxSouperDimanche.TabIndex = 12;
            // 
            // textBoxSouperLundi
            // 
            this.textBoxSouperLundi.Location = new System.Drawing.Point(119, 253);
            this.textBoxSouperLundi.Name = "textBoxSouperLundi";
            this.textBoxSouperLundi.Size = new System.Drawing.Size(138, 20);
            this.textBoxSouperLundi.TabIndex = 13;
            // 
            // textBoxSouperMardi
            // 
            this.textBoxSouperMardi.Location = new System.Drawing.Point(119, 298);
            this.textBoxSouperMardi.Name = "textBoxSouperMardi";
            this.textBoxSouperMardi.Size = new System.Drawing.Size(138, 20);
            this.textBoxSouperMardi.TabIndex = 14;
            // 
            // textBoxSouperMercredi
            // 
            this.textBoxSouperMercredi.Location = new System.Drawing.Point(119, 343);
            this.textBoxSouperMercredi.Name = "textBoxSouperMercredi";
            this.textBoxSouperMercredi.Size = new System.Drawing.Size(138, 20);
            this.textBoxSouperMercredi.TabIndex = 15;
            // 
            // textBoxSouperJeudi
            // 
            this.textBoxSouperJeudi.Location = new System.Drawing.Point(119, 393);
            this.textBoxSouperJeudi.Name = "textBoxSouperJeudi";
            this.textBoxSouperJeudi.Size = new System.Drawing.Size(138, 20);
            this.textBoxSouperJeudi.TabIndex = 16;
            // 
            // buttonSouperVendredi
            // 
            this.buttonSouperVendredi.Location = new System.Drawing.Point(289, 117);
            this.buttonSouperVendredi.Name = "buttonSouperVendredi";
            this.buttonSouperVendredi.Size = new System.Drawing.Size(75, 23);
            this.buttonSouperVendredi.TabIndex = 17;
            this.buttonSouperVendredi.Text = "Au hasard";
            this.buttonSouperVendredi.UseVisualStyleBackColor = true;
            this.buttonSouperVendredi.Click += new System.EventHandler(this.buttonSouperVendredi_Click);
            // 
            // buttonSouperSamedi
            // 
            this.buttonSouperSamedi.Location = new System.Drawing.Point(289, 163);
            this.buttonSouperSamedi.Name = "buttonSouperSamedi";
            this.buttonSouperSamedi.Size = new System.Drawing.Size(75, 23);
            this.buttonSouperSamedi.TabIndex = 18;
            this.buttonSouperSamedi.Text = "Au hasard";
            this.buttonSouperSamedi.UseVisualStyleBackColor = true;
            this.buttonSouperSamedi.Click += new System.EventHandler(this.buttonSouperSamedi_Click);
            // 
            // buttonSouperDimanche
            // 
            this.buttonSouperDimanche.Location = new System.Drawing.Point(289, 206);
            this.buttonSouperDimanche.Name = "buttonSouperDimanche";
            this.buttonSouperDimanche.Size = new System.Drawing.Size(75, 23);
            this.buttonSouperDimanche.TabIndex = 19;
            this.buttonSouperDimanche.Text = "Au hasard";
            this.buttonSouperDimanche.UseVisualStyleBackColor = true;
            this.buttonSouperDimanche.Click += new System.EventHandler(this.buttonSouperDimanche_Click);
            // 
            // buttonSouperLundi
            // 
            this.buttonSouperLundi.Location = new System.Drawing.Point(289, 253);
            this.buttonSouperLundi.Name = "buttonSouperLundi";
            this.buttonSouperLundi.Size = new System.Drawing.Size(75, 23);
            this.buttonSouperLundi.TabIndex = 20;
            this.buttonSouperLundi.Text = "Au hasard";
            this.buttonSouperLundi.UseVisualStyleBackColor = true;
            this.buttonSouperLundi.Click += new System.EventHandler(this.buttonSouperLundi_Click);
            // 
            // buttonSouperMardi
            // 
            this.buttonSouperMardi.Location = new System.Drawing.Point(289, 298);
            this.buttonSouperMardi.Name = "buttonSouperMardi";
            this.buttonSouperMardi.Size = new System.Drawing.Size(75, 23);
            this.buttonSouperMardi.TabIndex = 21;
            this.buttonSouperMardi.Text = "Au hasard";
            this.buttonSouperMardi.UseVisualStyleBackColor = true;
            this.buttonSouperMardi.Click += new System.EventHandler(this.buttonSouperMardi_Click);
            // 
            // buttonSouperMercredi
            // 
            this.buttonSouperMercredi.Location = new System.Drawing.Point(289, 343);
            this.buttonSouperMercredi.Name = "buttonSouperMercredi";
            this.buttonSouperMercredi.Size = new System.Drawing.Size(75, 23);
            this.buttonSouperMercredi.TabIndex = 22;
            this.buttonSouperMercredi.Text = "Au hasard";
            this.buttonSouperMercredi.UseVisualStyleBackColor = true;
            this.buttonSouperMercredi.Click += new System.EventHandler(this.buttonSouperMercredi_Click);
            // 
            // buttonSouperJeudi
            // 
            this.buttonSouperJeudi.Location = new System.Drawing.Point(289, 393);
            this.buttonSouperJeudi.Name = "buttonSouperJeudi";
            this.buttonSouperJeudi.Size = new System.Drawing.Size(75, 23);
            this.buttonSouperJeudi.TabIndex = 23;
            this.buttonSouperJeudi.Text = "Au hasard";
            this.buttonSouperJeudi.UseVisualStyleBackColor = true;
            this.buttonSouperJeudi.Click += new System.EventHandler(this.buttonSouperJeudi_Click);
            // 
            // buttonSouperAll
            // 
            this.buttonSouperAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonSouperAll.Location = new System.Drawing.Point(139, 477);
            this.buttonSouperAll.Name = "buttonSouperAll";
            this.buttonSouperAll.Size = new System.Drawing.Size(266, 51);
            this.buttonSouperAll.TabIndex = 24;
            this.buttonSouperAll.Text = "Générer tous les soupers";
            this.buttonSouperAll.UseVisualStyleBackColor = true;
            this.buttonSouperAll.Click += new System.EventHandler(this.buttonSouperAll_Click);
            // 
            // textBoxDinerSamedi
            // 
            this.textBoxDinerSamedi.Location = new System.Drawing.Point(589, 166);
            this.textBoxDinerSamedi.Name = "textBoxDinerSamedi";
            this.textBoxDinerSamedi.Size = new System.Drawing.Size(135, 20);
            this.textBoxDinerSamedi.TabIndex = 25;
            // 
            // textBoxDinerDimanche
            // 
            this.textBoxDinerDimanche.Location = new System.Drawing.Point(589, 209);
            this.textBoxDinerDimanche.Name = "textBoxDinerDimanche";
            this.textBoxDinerDimanche.Size = new System.Drawing.Size(135, 20);
            this.textBoxDinerDimanche.TabIndex = 26;
            // 
            // buttonDinerSamedi
            // 
            this.buttonDinerSamedi.Location = new System.Drawing.Point(745, 166);
            this.buttonDinerSamedi.Name = "buttonDinerSamedi";
            this.buttonDinerSamedi.Size = new System.Drawing.Size(75, 23);
            this.buttonDinerSamedi.TabIndex = 27;
            this.buttonDinerSamedi.Text = "Au hasard";
            this.buttonDinerSamedi.UseVisualStyleBackColor = true;
            this.buttonDinerSamedi.Click += new System.EventHandler(this.buttonDinerSamedi_Click);
            // 
            // buttonWeekAll
            // 
            this.buttonWeekAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonWeekAll.Location = new System.Drawing.Point(474, 477);
            this.buttonWeekAll.Name = "buttonWeekAll";
            this.buttonWeekAll.Size = new System.Drawing.Size(406, 145);
            this.buttonWeekAll.TabIndex = 31;
            this.buttonWeekAll.Text = "Générer la semaine";
            this.buttonWeekAll.UseVisualStyleBackColor = true;
            this.buttonWeekAll.Click += new System.EventHandler(this.buttonWeekAll_Click);
            // 
            // labelListe
            // 
            this.labelListe.AutoSize = true;
            this.labelListe.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.labelListe.Location = new System.Drawing.Point(1239, 63);
            this.labelListe.Name = "labelListe";
            this.labelListe.Size = new System.Drawing.Size(84, 37);
            this.labelListe.TabIndex = 32;
            this.labelListe.Text = "Liste";
            this.labelListe.Click += new System.EventHandler(this.labelListe_Click);
            // 
            // textBoxListeFruits
            // 
            this.textBoxListeFruits.Location = new System.Drawing.Point(924, 139);
            this.textBoxListeFruits.Name = "textBoxListeFruits";
            this.textBoxListeFruits.Size = new System.Drawing.Size(343, 81);
            this.textBoxListeFruits.TabIndex = 33;
            this.textBoxListeFruits.Text = "";
            this.textBoxListeFruits.TextChanged += new System.EventHandler(this.textBoxListe_TextChanged);
            // 
            // buttonOutExcel
            // 
            this.buttonOutExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonOutExcel.Location = new System.Drawing.Point(1162, 660);
            this.buttonOutExcel.Name = "buttonOutExcel";
            this.buttonOutExcel.Size = new System.Drawing.Size(266, 51);
            this.buttonOutExcel.TabIndex = 34;
            this.buttonOutExcel.Text = "Vers excel";
            this.buttonOutExcel.UseVisualStyleBackColor = true;
            this.buttonOutExcel.Click += new System.EventHandler(this.buttonOutExcel_Click);
            // 
            // textBoxListeViande
            // 
            this.textBoxListeViande.Location = new System.Drawing.Point(924, 275);
            this.textBoxListeViande.Name = "textBoxListeViande";
            this.textBoxListeViande.Size = new System.Drawing.Size(343, 81);
            this.textBoxListeViande.TabIndex = 36;
            this.textBoxListeViande.Text = "";
            this.textBoxListeViande.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // textBoxListeBoulangerie
            // 
            this.textBoxListeBoulangerie.Location = new System.Drawing.Point(924, 410);
            this.textBoxListeBoulangerie.Name = "textBoxListeBoulangerie";
            this.textBoxListeBoulangerie.Size = new System.Drawing.Size(343, 81);
            this.textBoxListeBoulangerie.TabIndex = 37;
            this.textBoxListeBoulangerie.Text = "";
            this.textBoxListeBoulangerie.TextChanged += new System.EventHandler(this.richTextBox2_TextChanged);
            // 
            // textBoxListeDejeuner
            // 
            this.textBoxListeDejeuner.Location = new System.Drawing.Point(924, 541);
            this.textBoxListeDejeuner.Name = "textBoxListeDejeuner";
            this.textBoxListeDejeuner.Size = new System.Drawing.Size(343, 81);
            this.textBoxListeDejeuner.TabIndex = 38;
            this.textBoxListeDejeuner.Text = "";
            this.textBoxListeDejeuner.TextChanged += new System.EventHandler(this.richTextBox3_TextChanged);
            // 
            // textBoxListeLait
            // 
            this.textBoxListeLait.Location = new System.Drawing.Point(1304, 139);
            this.textBoxListeLait.Name = "textBoxListeLait";
            this.textBoxListeLait.Size = new System.Drawing.Size(343, 81);
            this.textBoxListeLait.TabIndex = 39;
            this.textBoxListeLait.Text = "";
            // 
            // textBoxListePate
            // 
            this.textBoxListePate.Location = new System.Drawing.Point(1304, 275);
            this.textBoxListePate.Name = "textBoxListePate";
            this.textBoxListePate.Size = new System.Drawing.Size(343, 81);
            this.textBoxListePate.TabIndex = 40;
            this.textBoxListePate.Text = "";
            // 
            // textBoxListeGateau
            // 
            this.textBoxListeGateau.Location = new System.Drawing.Point(1304, 410);
            this.textBoxListeGateau.Name = "textBoxListeGateau";
            this.textBoxListeGateau.Size = new System.Drawing.Size(343, 81);
            this.textBoxListeGateau.TabIndex = 41;
            this.textBoxListeGateau.Text = "";
            // 
            // textBoxListeCongelo
            // 
            this.textBoxListeCongelo.Location = new System.Drawing.Point(1304, 541);
            this.textBoxListeCongelo.Name = "textBoxListeCongelo";
            this.textBoxListeCongelo.Size = new System.Drawing.Size(343, 81);
            this.textBoxListeCongelo.TabIndex = 42;
            this.textBoxListeCongelo.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1045, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 43;
            this.label3.Text = "Fruits et légumes";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1068, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 44;
            this.label4.Text = "Viande";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1057, 384);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Boulangerie";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1061, 515);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 46;
            this.label6.Text = "Déjeuners";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1455, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 47;
            this.label7.Text = "Produits laitiers";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1464, 249);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 48;
            this.label8.Text = "Pâtes et riz";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1470, 384);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 49;
            this.label9.Text = "Gateaux";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1433, 515);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 13);
            this.label10.TabIndex = 50;
            this.label10.Text = "Boissons et congélateur";
            // 
            // comboBoxSouperVendredi
            // 
            this.comboBoxSouperVendredi.FormattingEnabled = true;
            this.comboBoxSouperVendredi.Items.AddRange(new object[] {
            "Très pressé",
            "Soir de cours",
            "Soir sans cours",
            "Fin de semaine",
            "Invités",
            "Diner"});
            this.comboBoxSouperVendredi.Location = new System.Drawing.Point(385, 117);
            this.comboBoxSouperVendredi.Name = "comboBoxSouperVendredi";
            this.comboBoxSouperVendredi.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSouperVendredi.TabIndex = 52;
            // 
            // comboBoxSouperSamedi
            // 
            this.comboBoxSouperSamedi.FormattingEnabled = true;
            this.comboBoxSouperSamedi.Items.AddRange(new object[] {
            "Très pressé",
            "Soir de cours",
            "Soir sans cours",
            "Fin de semaine",
            "Invités",
            "Diner"});
            this.comboBoxSouperSamedi.Location = new System.Drawing.Point(385, 163);
            this.comboBoxSouperSamedi.Name = "comboBoxSouperSamedi";
            this.comboBoxSouperSamedi.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSouperSamedi.TabIndex = 53;
            // 
            // comboBoxSouperDimanche
            // 
            this.comboBoxSouperDimanche.FormattingEnabled = true;
            this.comboBoxSouperDimanche.Items.AddRange(new object[] {
            "Très pressé",
            "Soir de cours",
            "Soir sans cours",
            "Fin de semaine",
            "Invités",
            "Diner"});
            this.comboBoxSouperDimanche.Location = new System.Drawing.Point(385, 208);
            this.comboBoxSouperDimanche.Name = "comboBoxSouperDimanche";
            this.comboBoxSouperDimanche.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSouperDimanche.TabIndex = 54;
            // 
            // comboBoxSouperLundi
            // 
            this.comboBoxSouperLundi.FormattingEnabled = true;
            this.comboBoxSouperLundi.Items.AddRange(new object[] {
            "Très pressé",
            "Soir de cours",
            "Soir sans cours",
            "Fin de semaine",
            "Invités",
            "Diner"});
            this.comboBoxSouperLundi.Location = new System.Drawing.Point(385, 258);
            this.comboBoxSouperLundi.Name = "comboBoxSouperLundi";
            this.comboBoxSouperLundi.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSouperLundi.TabIndex = 55;
            // 
            // comboBoxSouperMardi
            // 
            this.comboBoxSouperMardi.FormattingEnabled = true;
            this.comboBoxSouperMardi.Items.AddRange(new object[] {
            "Très pressé",
            "Soir de cours",
            "Soir sans cours",
            "Fin de semaine",
            "Invités",
            "Diner"});
            this.comboBoxSouperMardi.Location = new System.Drawing.Point(385, 298);
            this.comboBoxSouperMardi.Name = "comboBoxSouperMardi";
            this.comboBoxSouperMardi.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSouperMardi.TabIndex = 56;
            // 
            // comboBoxSouperMercredi
            // 
            this.comboBoxSouperMercredi.FormattingEnabled = true;
            this.comboBoxSouperMercredi.Items.AddRange(new object[] {
            "Très pressé",
            "Soir de cours",
            "Soir sans cours",
            "Fin de semaine",
            "Invités",
            "Diner"});
            this.comboBoxSouperMercredi.Location = new System.Drawing.Point(385, 343);
            this.comboBoxSouperMercredi.Name = "comboBoxSouperMercredi";
            this.comboBoxSouperMercredi.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSouperMercredi.TabIndex = 57;
            // 
            // comboBoxSouperJeudi
            // 
            this.comboBoxSouperJeudi.FormattingEnabled = true;
            this.comboBoxSouperJeudi.Items.AddRange(new object[] {
            "Très pressé",
            "Soir de cours",
            "Soir sans cours",
            "Fin de semaine",
            "Invités",
            "Diner"});
            this.comboBoxSouperJeudi.Location = new System.Drawing.Point(385, 393);
            this.comboBoxSouperJeudi.Name = "comboBoxSouperJeudi";
            this.comboBoxSouperJeudi.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSouperJeudi.TabIndex = 58;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Excel Files (*.xlsx)|*.xlsx";
            // 
            // buttonDinerDimanche
            // 
            this.buttonDinerDimanche.Location = new System.Drawing.Point(745, 209);
            this.buttonDinerDimanche.Name = "buttonDinerDimanche";
            this.buttonDinerDimanche.Size = new System.Drawing.Size(75, 23);
            this.buttonDinerDimanche.TabIndex = 60;
            this.buttonDinerDimanche.Text = "Au hasard";
            this.buttonDinerDimanche.UseVisualStyleBackColor = true;
            this.buttonDinerDimanche.Click += new System.EventHandler(this.buttonDinerDimanche_Click);
            // 
            // buttonDinerAll
            // 
            this.buttonDinerAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonDinerAll.Location = new System.Drawing.Point(589, 325);
            this.buttonDinerAll.Name = "buttonDinerAll";
            this.buttonDinerAll.Size = new System.Drawing.Size(264, 51);
            this.buttonDinerAll.TabIndex = 61;
            this.buttonDinerAll.Text = "Générer tous les dîners";
            this.buttonDinerAll.UseVisualStyleBackColor = true;
            this.buttonDinerAll.Click += new System.EventHandler(this.buttonDinerAll_Click);
            // 
            // checkBoxVendredi
            // 
            this.checkBoxVendredi.AutoSize = true;
            this.checkBoxVendredi.Location = new System.Drawing.Point(13, 120);
            this.checkBoxVendredi.Name = "checkBoxVendredi";
            this.checkBoxVendredi.Size = new System.Drawing.Size(15, 14);
            this.checkBoxVendredi.TabIndex = 62;
            this.checkBoxVendredi.UseVisualStyleBackColor = true;
            this.checkBoxVendredi.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBoxSamedi
            // 
            this.checkBoxSamedi.AutoSize = true;
            this.checkBoxSamedi.Location = new System.Drawing.Point(12, 162);
            this.checkBoxSamedi.Name = "checkBoxSamedi";
            this.checkBoxSamedi.Size = new System.Drawing.Size(15, 14);
            this.checkBoxSamedi.TabIndex = 63;
            this.checkBoxSamedi.UseVisualStyleBackColor = true;
            // 
            // checkBoxDimanche
            // 
            this.checkBoxDimanche.AutoSize = true;
            this.checkBoxDimanche.Location = new System.Drawing.Point(13, 208);
            this.checkBoxDimanche.Name = "checkBoxDimanche";
            this.checkBoxDimanche.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDimanche.TabIndex = 64;
            this.checkBoxDimanche.UseVisualStyleBackColor = true;
            // 
            // checkBoxLundi
            // 
            this.checkBoxLundi.AutoSize = true;
            this.checkBoxLundi.Location = new System.Drawing.Point(13, 252);
            this.checkBoxLundi.Name = "checkBoxLundi";
            this.checkBoxLundi.Size = new System.Drawing.Size(15, 14);
            this.checkBoxLundi.TabIndex = 65;
            this.checkBoxLundi.UseVisualStyleBackColor = true;
            // 
            // checkBoxMardi
            // 
            this.checkBoxMardi.AutoSize = true;
            this.checkBoxMardi.Location = new System.Drawing.Point(12, 297);
            this.checkBoxMardi.Name = "checkBoxMardi";
            this.checkBoxMardi.Size = new System.Drawing.Size(15, 14);
            this.checkBoxMardi.TabIndex = 66;
            this.checkBoxMardi.UseVisualStyleBackColor = true;
            // 
            // checkBoxMercredi
            // 
            this.checkBoxMercredi.AutoSize = true;
            this.checkBoxMercredi.Location = new System.Drawing.Point(12, 345);
            this.checkBoxMercredi.Name = "checkBoxMercredi";
            this.checkBoxMercredi.Size = new System.Drawing.Size(15, 14);
            this.checkBoxMercredi.TabIndex = 67;
            this.checkBoxMercredi.UseVisualStyleBackColor = true;
            // 
            // checkBoxJeudi
            // 
            this.checkBoxJeudi.AutoSize = true;
            this.checkBoxJeudi.Location = new System.Drawing.Point(12, 395);
            this.checkBoxJeudi.Name = "checkBoxJeudi";
            this.checkBoxJeudi.Size = new System.Drawing.Size(15, 14);
            this.checkBoxJeudi.TabIndex = 68;
            this.checkBoxJeudi.UseVisualStyleBackColor = true;
            // 
            // checkBoxDinerSamedi
            // 
            this.checkBoxDinerSamedi.AutoSize = true;
            this.checkBoxDinerSamedi.Location = new System.Drawing.Point(838, 169);
            this.checkBoxDinerSamedi.Name = "checkBoxDinerSamedi";
            this.checkBoxDinerSamedi.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDinerSamedi.TabIndex = 69;
            this.checkBoxDinerSamedi.UseVisualStyleBackColor = true;
            // 
            // checkBoxDinerDimanche
            // 
            this.checkBoxDinerDimanche.AutoSize = true;
            this.checkBoxDinerDimanche.Location = new System.Drawing.Point(838, 211);
            this.checkBoxDinerDimanche.Name = "checkBoxDinerDimanche";
            this.checkBoxDinerDimanche.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDinerDimanche.TabIndex = 70;
            this.checkBoxDinerDimanche.UseVisualStyleBackColor = true;
            // 
            // SelectSouper
            // 
            this.SelectSouper.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.SelectSouper.Location = new System.Drawing.Point(13, 492);
            this.SelectSouper.Name = "SelectSouper";
            this.SelectSouper.Size = new System.Drawing.Size(108, 23);
            this.SelectSouper.TabIndex = 71;
            this.SelectSouper.Text = "Tout sélectionner";
            this.SelectSouper.UseVisualStyleBackColor = true;
            this.SelectSouper.Click += new System.EventHandler(this.SelectSouper_Click);
            // 
            // SelectDiner
            // 
            this.SelectDiner.Location = new System.Drawing.Point(741, 273);
            this.SelectDiner.Name = "SelectDiner";
            this.SelectDiner.Size = new System.Drawing.Size(112, 23);
            this.SelectDiner.TabIndex = 72;
            this.SelectDiner.Text = "Tout sélectionner";
            this.SelectDiner.UseVisualStyleBackColor = true;
            this.SelectDiner.Click += new System.EventHandler(this.SelectDiner_Click);
            // 
            // Generateur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1758, 805);
            this.Controls.Add(this.SelectDiner);
            this.Controls.Add(this.SelectSouper);
            this.Controls.Add(this.checkBoxDinerDimanche);
            this.Controls.Add(this.checkBoxDinerSamedi);
            this.Controls.Add(this.checkBoxJeudi);
            this.Controls.Add(this.checkBoxMercredi);
            this.Controls.Add(this.checkBoxMardi);
            this.Controls.Add(this.checkBoxLundi);
            this.Controls.Add(this.checkBoxDimanche);
            this.Controls.Add(this.checkBoxSamedi);
            this.Controls.Add(this.checkBoxVendredi);
            this.Controls.Add(this.buttonDinerAll);
            this.Controls.Add(this.buttonDinerDimanche);
            this.Controls.Add(this.comboBoxSouperJeudi);
            this.Controls.Add(this.comboBoxSouperMercredi);
            this.Controls.Add(this.comboBoxSouperMardi);
            this.Controls.Add(this.comboBoxSouperLundi);
            this.Controls.Add(this.comboBoxSouperDimanche);
            this.Controls.Add(this.comboBoxSouperSamedi);
            this.Controls.Add(this.comboBoxSouperVendredi);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxListeCongelo);
            this.Controls.Add(this.textBoxListeGateau);
            this.Controls.Add(this.textBoxListePate);
            this.Controls.Add(this.textBoxListeLait);
            this.Controls.Add(this.textBoxListeDejeuner);
            this.Controls.Add(this.textBoxListeBoulangerie);
            this.Controls.Add(this.textBoxListeViande);
            this.Controls.Add(this.buttonOutExcel);
            this.Controls.Add(this.textBoxListeFruits);
            this.Controls.Add(this.labelListe);
            this.Controls.Add(this.buttonWeekAll);
            this.Controls.Add(this.buttonDinerSamedi);
            this.Controls.Add(this.textBoxDinerDimanche);
            this.Controls.Add(this.textBoxDinerSamedi);
            this.Controls.Add(this.buttonSouperAll);
            this.Controls.Add(this.buttonSouperJeudi);
            this.Controls.Add(this.buttonSouperMercredi);
            this.Controls.Add(this.buttonSouperMardi);
            this.Controls.Add(this.buttonSouperLundi);
            this.Controls.Add(this.buttonSouperDimanche);
            this.Controls.Add(this.buttonSouperSamedi);
            this.Controls.Add(this.buttonSouperVendredi);
            this.Controls.Add(this.textBoxSouperJeudi);
            this.Controls.Add(this.textBoxSouperMercredi);
            this.Controls.Add(this.textBoxSouperMardi);
            this.Controls.Add(this.textBoxSouperLundi);
            this.Controls.Add(this.textBoxSouperDimanche);
            this.Controls.Add(this.textBoxSouperSamedi);
            this.Controls.Add(this.textBoxSouperVendredi);
            this.Controls.Add(this.labelDiner);
            this.Controls.Add(this.labelJeudi);
            this.Controls.Add(this.labelMercredi);
            this.Controls.Add(this.labelMardi);
            this.Controls.Add(this.labelLundi);
            this.Controls.Add(this.labelDimanche);
            this.Controls.Add(this.labelSamedi);
            this.Controls.Add(this.labelVendredi);
            this.Controls.Add(this.labelSouper);
            this.Controls.Add(this.dateTimePicker1);
            this.Name = "Generateur";
            this.Text = "Générateur de liste";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label labelSouper;
        private System.Windows.Forms.Label labelVendredi;
        private System.Windows.Forms.Label labelSamedi;
        private System.Windows.Forms.Label labelDimanche;
        private System.Windows.Forms.Label labelLundi;
        private System.Windows.Forms.Label labelMardi;
        private System.Windows.Forms.Label labelMercredi;
        private System.Windows.Forms.Label labelJeudi;
        private System.Windows.Forms.Label labelDiner;
        private System.Windows.Forms.TextBox textBoxSouperVendredi;
        private System.Windows.Forms.TextBox textBoxSouperSamedi;
        private System.Windows.Forms.TextBox textBoxSouperDimanche;
        private System.Windows.Forms.TextBox textBoxSouperLundi;
        private System.Windows.Forms.TextBox textBoxSouperMardi;
        private System.Windows.Forms.TextBox textBoxSouperMercredi;
        private System.Windows.Forms.TextBox textBoxSouperJeudi;
        private System.Windows.Forms.Button buttonSouperVendredi;
        private System.Windows.Forms.Button buttonSouperSamedi;
        private System.Windows.Forms.Button buttonSouperDimanche;
        private System.Windows.Forms.Button buttonSouperLundi;
        private System.Windows.Forms.Button buttonSouperMardi;
        private System.Windows.Forms.Button buttonSouperMercredi;
        private System.Windows.Forms.Button buttonSouperJeudi;
        private System.Windows.Forms.Button buttonSouperAll;
        private System.Windows.Forms.TextBox textBoxDinerSamedi;
        private System.Windows.Forms.TextBox textBoxDinerDimanche;
        private System.Windows.Forms.Button buttonDinerSamedi;
        private System.Windows.Forms.Button buttonDinerDimanche;
        private System.Windows.Forms.Button buttonWeekAll;
        private System.Windows.Forms.Label labelListe;
        private System.Windows.Forms.RichTextBox textBoxListeFruits;
        private System.Windows.Forms.Button buttonOutExcel;
        private System.Windows.Forms.RichTextBox textBoxListeViande;
        private System.Windows.Forms.RichTextBox textBoxListeBoulangerie;
        private System.Windows.Forms.RichTextBox textBoxListeDejeuner;
        private System.Windows.Forms.RichTextBox textBoxListeLait;
        private System.Windows.Forms.RichTextBox textBoxListePate;
        private System.Windows.Forms.RichTextBox textBoxListeGateau;
        private System.Windows.Forms.RichTextBox textBoxListeCongelo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxSouperVendredi;
        private System.Windows.Forms.ComboBox comboBoxSouperSamedi;
        private System.Windows.Forms.ComboBox comboBoxSouperDimanche;
        private System.Windows.Forms.ComboBox comboBoxSouperLundi;
        private System.Windows.Forms.ComboBox comboBoxSouperMardi;
        private System.Windows.Forms.ComboBox comboBoxSouperMercredi;
        private System.Windows.Forms.ComboBox comboBoxSouperJeudi;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button buttonDinerAll;
        private System.Windows.Forms.CheckBox checkBoxVendredi;
        private System.Windows.Forms.CheckBox checkBoxSamedi;
        private System.Windows.Forms.CheckBox checkBoxDimanche;
        private System.Windows.Forms.CheckBox checkBoxLundi;
        private System.Windows.Forms.CheckBox checkBoxMardi;
        private System.Windows.Forms.CheckBox checkBoxMercredi;
        private System.Windows.Forms.CheckBox checkBoxJeudi;
        private System.Windows.Forms.CheckBox checkBoxDinerSamedi;
        private System.Windows.Forms.CheckBox checkBoxDinerDimanche;
        private System.Windows.Forms.Button SelectSouper;
        private System.Windows.Forms.Button SelectDiner;
    }
}