﻿namespace Liste_Epiceri
{
    partial class Saisi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.textBoxBoulangerie1 = new System.Windows.Forms.TextBox();
            this.textBoxViande3 = new System.Windows.Forms.TextBox();
            this.textBoxViande2 = new System.Windows.Forms.TextBox();
            this.textBoxViande1 = new System.Windows.Forms.TextBox();
            this.textBoxFruit3 = new System.Windows.Forms.TextBox();
            this.textBoxFruit2 = new System.Windows.Forms.TextBox();
            this.textBoxFruit1 = new System.Windows.Forms.TextBox();
            this.labelStyle = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelVitesse = new System.Windows.Forms.Label();
            this.labelNom = new System.Windows.Forms.Label();
            this.labelSousStyle = new System.Windows.Forms.Label();
            this.comboBoxStyle = new System.Windows.Forms.ComboBox();
            this.comboBoxSousStyle = new System.Windows.Forms.ComboBox();
            this.comboBoxVitesse = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxBoulangerie2 = new System.Windows.Forms.TextBox();
            this.textBoxDejeuner1 = new System.Windows.Forms.TextBox();
            this.textBoxPate2 = new System.Windows.Forms.TextBox();
            this.textBoxDejeuner2 = new System.Windows.Forms.TextBox();
            this.textBoxPate1 = new System.Windows.Forms.TextBox();
            this.textBoxPate3 = new System.Windows.Forms.TextBox();
            this.textBoxCongelo1 = new System.Windows.Forms.TextBox();
            this.textBoxGateau4 = new System.Windows.Forms.TextBox();
            this.textBoxGateau3 = new System.Windows.Forms.TextBox();
            this.textBoxGateau2 = new System.Windows.Forms.TextBox();
            this.textBoxGateau1 = new System.Windows.Forms.TextBox();
            this.textBoxLaitier3 = new System.Windows.Forms.TextBox();
            this.textBoxLaitier2 = new System.Windows.Forms.TextBox();
            this.textBoxLaitier1 = new System.Windows.Forms.TextBox();
            this.textBoxCongelo2 = new System.Windows.Forms.TextBox();
            this.textBoxCongelo3 = new System.Windows.Forms.TextBox();
            this.buttonRecord = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxNom
            // 
            this.textBoxNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.textBoxNom.Location = new System.Drawing.Point(705, 61);
            this.textBoxNom.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(877, 32);
            this.textBoxNom.TabIndex = 1;
            this.textBoxNom.TextChanged += new System.EventHandler(this.textBoxNom_TextChanged);
            // 
            // textBoxBoulangerie1
            // 
            this.textBoxBoulangerie1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxBoulangerie1.Location = new System.Drawing.Point(290, 681);
            this.textBoxBoulangerie1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxBoulangerie1.Name = "textBoxBoulangerie1";
            this.textBoxBoulangerie1.Size = new System.Drawing.Size(148, 24);
            this.textBoxBoulangerie1.TabIndex = 2;
            // 
            // textBoxViande3
            // 
            this.textBoxViande3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxViande3.Location = new System.Drawing.Point(290, 615);
            this.textBoxViande3.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxViande3.Name = "textBoxViande3";
            this.textBoxViande3.Size = new System.Drawing.Size(148, 24);
            this.textBoxViande3.TabIndex = 3;
            // 
            // textBoxViande2
            // 
            this.textBoxViande2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxViande2.Location = new System.Drawing.Point(290, 579);
            this.textBoxViande2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxViande2.Name = "textBoxViande2";
            this.textBoxViande2.Size = new System.Drawing.Size(148, 24);
            this.textBoxViande2.TabIndex = 4;
            // 
            // textBoxViande1
            // 
            this.textBoxViande1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxViande1.Location = new System.Drawing.Point(290, 543);
            this.textBoxViande1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxViande1.Name = "textBoxViande1";
            this.textBoxViande1.Size = new System.Drawing.Size(148, 24);
            this.textBoxViande1.TabIndex = 5;
            // 
            // textBoxFruit3
            // 
            this.textBoxFruit3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxFruit3.Location = new System.Drawing.Point(290, 461);
            this.textBoxFruit3.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxFruit3.Name = "textBoxFruit3";
            this.textBoxFruit3.Size = new System.Drawing.Size(148, 24);
            this.textBoxFruit3.TabIndex = 6;
            // 
            // textBoxFruit2
            // 
            this.textBoxFruit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxFruit2.Location = new System.Drawing.Point(290, 425);
            this.textBoxFruit2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxFruit2.Name = "textBoxFruit2";
            this.textBoxFruit2.Size = new System.Drawing.Size(148, 24);
            this.textBoxFruit2.TabIndex = 7;
            // 
            // textBoxFruit1
            // 
            this.textBoxFruit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxFruit1.Location = new System.Drawing.Point(290, 389);
            this.textBoxFruit1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxFruit1.Name = "textBoxFruit1";
            this.textBoxFruit1.Size = new System.Drawing.Size(148, 24);
            this.textBoxFruit1.TabIndex = 8;
            this.textBoxFruit1.TextChanged += new System.EventHandler(this.textBoxFruit1_TextChanged);
            // 
            // labelStyle
            // 
            this.labelStyle.AutoSize = true;
            this.labelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.labelStyle.Location = new System.Drawing.Point(104, 174);
            this.labelStyle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStyle.Name = "labelStyle";
            this.labelStyle.Size = new System.Drawing.Size(75, 31);
            this.labelStyle.TabIndex = 11;
            this.labelStyle.Text = "Style";
            this.labelStyle.Click += new System.EventHandler(this.labelStyle_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label2.Location = new System.Drawing.Point(76, 685);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 18);
            this.label2.TabIndex = 12;
            this.label2.Text = "Boulangerie";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label3.Location = new System.Drawing.Point(76, 547);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 18);
            this.label3.TabIndex = 13;
            this.label3.Text = "Viande";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label4.Location = new System.Drawing.Point(76, 393);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 18);
            this.label4.TabIndex = 14;
            this.label4.Text = "Fruits et Légumes";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label5.Location = new System.Drawing.Point(808, 298);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 31);
            this.label5.TabIndex = 15;
            this.label5.Text = "Ingrédients";
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(174, 269);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(1500, 4);
            this.label6.TabIndex = 16;
            // 
            // labelVitesse
            // 
            this.labelVitesse.AutoSize = true;
            this.labelVitesse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelVitesse.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.labelVitesse.Location = new System.Drawing.Point(1179, 179);
            this.labelVitesse.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelVitesse.Name = "labelVitesse";
            this.labelVitesse.Size = new System.Drawing.Size(223, 26);
            this.labelVitesse.TabIndex = 17;
            this.labelVitesse.Text = "Temps de préparation";
            this.labelVitesse.Click += new System.EventHandler(this.labelVitesse_Click);
            // 
            // labelNom
            // 
            this.labelNom.AutoSize = true;
            this.labelNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F);
            this.labelNom.Location = new System.Drawing.Point(196, 44);
            this.labelNom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNom.Name = "labelNom";
            this.labelNom.Size = new System.Drawing.Size(320, 44);
            this.labelNom.TabIndex = 18;
            this.labelNom.Text = "Nom de la recette";
            this.labelNom.Click += new System.EventHandler(this.labelNom_Click);
            // 
            // labelSousStyle
            // 
            this.labelSousStyle.AutoSize = true;
            this.labelSousStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.labelSousStyle.Location = new System.Drawing.Point(588, 174);
            this.labelSousStyle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSousStyle.Name = "labelSousStyle";
            this.labelSousStyle.Size = new System.Drawing.Size(144, 31);
            this.labelSousStyle.TabIndex = 19;
            this.labelSousStyle.Text = "Sous Style";
            this.labelSousStyle.Click += new System.EventHandler(this.labelSousStyle_Click);
            // 
            // comboBoxStyle
            // 
            this.comboBoxStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.comboBoxStyle.FormattingEnabled = true;
            this.comboBoxStyle.Items.AddRange(new object[] {
            "Souper",
            "Diner",
            "Déjeuner",
            "Entrée",
            "Dessert",
            "Soupe",
            "Salades"});
            this.comboBoxStyle.Location = new System.Drawing.Point(225, 174);
            this.comboBoxStyle.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxStyle.Name = "comboBoxStyle";
            this.comboBoxStyle.Size = new System.Drawing.Size(253, 33);
            this.comboBoxStyle.TabIndex = 21;
            this.comboBoxStyle.SelectedIndexChanged += new System.EventHandler(this.comboBoxStyle_SelectedIndexChanged);
            // 
            // comboBoxSousStyle
            // 
            this.comboBoxSousStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.comboBoxSousStyle.FormattingEnabled = true;
            this.comboBoxSousStyle.Items.AddRange(new object[] {
            "",
            "Pâtes avec viande",
            "Pâtes sans viande",
            "Autres viandes",
            "Mijoteuse",
            "Pizzas",
            "Poisson",
            "Poulet"});
            this.comboBoxSousStyle.Location = new System.Drawing.Point(814, 174);
            this.comboBoxSousStyle.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxSousStyle.Name = "comboBoxSousStyle";
            this.comboBoxSousStyle.Size = new System.Drawing.Size(324, 33);
            this.comboBoxSousStyle.TabIndex = 23;
            this.comboBoxSousStyle.SelectedIndexChanged += new System.EventHandler(this.comboBoxSousStyle_SelectedIndexChanged);
            // 
            // comboBoxVitesse
            // 
            this.comboBoxVitesse.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.comboBoxVitesse.FormattingEnabled = true;
            this.comboBoxVitesse.Items.AddRange(new object[] {
            "Très pressé",
            "Soir de cours",
            "Soir sans cours",
            "Fin de semaine",
            "Invités",
            "Diner"});
            this.comboBoxVitesse.Location = new System.Drawing.Point(1522, 174);
            this.comboBoxVitesse.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxVitesse.Name = "comboBoxVitesse";
            this.comboBoxVitesse.Size = new System.Drawing.Size(276, 33);
            this.comboBoxVitesse.TabIndex = 24;
            this.comboBoxVitesse.SelectedIndexChanged += new System.EventHandler(this.comboBoxVitesse_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label1.Location = new System.Drawing.Point(706, 393);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 18);
            this.label1.TabIndex = 25;
            this.label1.Text = "Déjeuner";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label7.Location = new System.Drawing.Point(706, 493);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 18);
            this.label7.TabIndex = 26;
            this.label7.Text = "Produits laitiers";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label10.Location = new System.Drawing.Point(706, 649);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 18);
            this.label10.TabIndex = 29;
            this.label10.Text = "Pâtes et riz";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label11.Location = new System.Drawing.Point(1314, 393);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 18);
            this.label11.TabIndex = 30;
            this.label11.Text = "Gâteaux et jus";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label12.Location = new System.Drawing.Point(1314, 590);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 18);
            this.label12.TabIndex = 31;
            this.label12.Text = "Congélateur";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // textBoxBoulangerie2
            // 
            this.textBoxBoulangerie2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxBoulangerie2.Location = new System.Drawing.Point(290, 717);
            this.textBoxBoulangerie2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxBoulangerie2.Name = "textBoxBoulangerie2";
            this.textBoxBoulangerie2.Size = new System.Drawing.Size(148, 24);
            this.textBoxBoulangerie2.TabIndex = 32;
            // 
            // textBoxDejeuner1
            // 
            this.textBoxDejeuner1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxDejeuner1.Location = new System.Drawing.Point(902, 389);
            this.textBoxDejeuner1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDejeuner1.Name = "textBoxDejeuner1";
            this.textBoxDejeuner1.Size = new System.Drawing.Size(148, 24);
            this.textBoxDejeuner1.TabIndex = 33;
            // 
            // textBoxPate2
            // 
            this.textBoxPate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxPate2.Location = new System.Drawing.Point(902, 681);
            this.textBoxPate2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPate2.Name = "textBoxPate2";
            this.textBoxPate2.Size = new System.Drawing.Size(148, 24);
            this.textBoxPate2.TabIndex = 34;
            // 
            // textBoxDejeuner2
            // 
            this.textBoxDejeuner2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxDejeuner2.Location = new System.Drawing.Point(902, 425);
            this.textBoxDejeuner2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDejeuner2.Name = "textBoxDejeuner2";
            this.textBoxDejeuner2.Size = new System.Drawing.Size(148, 24);
            this.textBoxDejeuner2.TabIndex = 35;
            // 
            // textBoxPate1
            // 
            this.textBoxPate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxPate1.Location = new System.Drawing.Point(902, 645);
            this.textBoxPate1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPate1.Name = "textBoxPate1";
            this.textBoxPate1.Size = new System.Drawing.Size(148, 24);
            this.textBoxPate1.TabIndex = 36;
            // 
            // textBoxPate3
            // 
            this.textBoxPate3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxPate3.Location = new System.Drawing.Point(902, 717);
            this.textBoxPate3.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPate3.Name = "textBoxPate3";
            this.textBoxPate3.Size = new System.Drawing.Size(148, 24);
            this.textBoxPate3.TabIndex = 37;
            // 
            // textBoxCongelo1
            // 
            this.textBoxCongelo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxCongelo1.Location = new System.Drawing.Point(1498, 580);
            this.textBoxCongelo1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCongelo1.Name = "textBoxCongelo1";
            this.textBoxCongelo1.Size = new System.Drawing.Size(148, 24);
            this.textBoxCongelo1.TabIndex = 38;
            // 
            // textBoxGateau4
            // 
            this.textBoxGateau4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxGateau4.Location = new System.Drawing.Point(1498, 501);
            this.textBoxGateau4.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxGateau4.Name = "textBoxGateau4";
            this.textBoxGateau4.Size = new System.Drawing.Size(148, 24);
            this.textBoxGateau4.TabIndex = 39;
            // 
            // textBoxGateau3
            // 
            this.textBoxGateau3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxGateau3.Location = new System.Drawing.Point(1498, 465);
            this.textBoxGateau3.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxGateau3.Name = "textBoxGateau3";
            this.textBoxGateau3.Size = new System.Drawing.Size(148, 24);
            this.textBoxGateau3.TabIndex = 40;
            // 
            // textBoxGateau2
            // 
            this.textBoxGateau2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxGateau2.Location = new System.Drawing.Point(1498, 429);
            this.textBoxGateau2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxGateau2.Name = "textBoxGateau2";
            this.textBoxGateau2.Size = new System.Drawing.Size(148, 24);
            this.textBoxGateau2.TabIndex = 41;
            // 
            // textBoxGateau1
            // 
            this.textBoxGateau1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxGateau1.Location = new System.Drawing.Point(1498, 393);
            this.textBoxGateau1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxGateau1.Name = "textBoxGateau1";
            this.textBoxGateau1.Size = new System.Drawing.Size(148, 24);
            this.textBoxGateau1.TabIndex = 42;
            // 
            // textBoxLaitier3
            // 
            this.textBoxLaitier3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxLaitier3.Location = new System.Drawing.Point(902, 561);
            this.textBoxLaitier3.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLaitier3.Name = "textBoxLaitier3";
            this.textBoxLaitier3.Size = new System.Drawing.Size(148, 24);
            this.textBoxLaitier3.TabIndex = 43;
            // 
            // textBoxLaitier2
            // 
            this.textBoxLaitier2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxLaitier2.Location = new System.Drawing.Point(902, 525);
            this.textBoxLaitier2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLaitier2.Name = "textBoxLaitier2";
            this.textBoxLaitier2.Size = new System.Drawing.Size(148, 24);
            this.textBoxLaitier2.TabIndex = 44;
            // 
            // textBoxLaitier1
            // 
            this.textBoxLaitier1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxLaitier1.Location = new System.Drawing.Point(902, 489);
            this.textBoxLaitier1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLaitier1.Name = "textBoxLaitier1";
            this.textBoxLaitier1.Size = new System.Drawing.Size(148, 24);
            this.textBoxLaitier1.TabIndex = 45;
            // 
            // textBoxCongelo2
            // 
            this.textBoxCongelo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxCongelo2.Location = new System.Drawing.Point(1498, 616);
            this.textBoxCongelo2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCongelo2.Name = "textBoxCongelo2";
            this.textBoxCongelo2.Size = new System.Drawing.Size(148, 24);
            this.textBoxCongelo2.TabIndex = 46;
            // 
            // textBoxCongelo3
            // 
            this.textBoxCongelo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.textBoxCongelo3.Location = new System.Drawing.Point(1498, 652);
            this.textBoxCongelo3.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCongelo3.Name = "textBoxCongelo3";
            this.textBoxCongelo3.Size = new System.Drawing.Size(148, 24);
            this.textBoxCongelo3.TabIndex = 47;
            // 
            // buttonRecord
            // 
            this.buttonRecord.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.buttonRecord.Location = new System.Drawing.Point(1474, 717);
            this.buttonRecord.Margin = new System.Windows.Forms.Padding(4);
            this.buttonRecord.Name = "buttonRecord";
            this.buttonRecord.Size = new System.Drawing.Size(339, 72);
            this.buttonRecord.TabIndex = 48;
            this.buttonRecord.Text = "Enregistrer la recette";
            this.buttonRecord.UseVisualStyleBackColor = false;
            this.buttonRecord.Click += new System.EventHandler(this.buttonRecord_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(1624, 61);
            this.buttonSearch.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(150, 32);
            this.buttonSearch.TabIndex = 49;
            this.buttonSearch.Text = "Rechercher";
            this.buttonSearch.UseVisualStyleBackColor = true;
            // 
            // Saisi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1870, 871);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.buttonRecord);
            this.Controls.Add(this.textBoxCongelo3);
            this.Controls.Add(this.textBoxCongelo2);
            this.Controls.Add(this.textBoxLaitier1);
            this.Controls.Add(this.textBoxLaitier2);
            this.Controls.Add(this.textBoxLaitier3);
            this.Controls.Add(this.textBoxGateau1);
            this.Controls.Add(this.textBoxGateau2);
            this.Controls.Add(this.textBoxGateau3);
            this.Controls.Add(this.textBoxGateau4);
            this.Controls.Add(this.textBoxCongelo1);
            this.Controls.Add(this.textBoxPate3);
            this.Controls.Add(this.textBoxPate1);
            this.Controls.Add(this.textBoxDejeuner2);
            this.Controls.Add(this.textBoxPate2);
            this.Controls.Add(this.textBoxDejeuner1);
            this.Controls.Add(this.textBoxBoulangerie2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxVitesse);
            this.Controls.Add(this.comboBoxSousStyle);
            this.Controls.Add(this.comboBoxStyle);
            this.Controls.Add(this.labelSousStyle);
            this.Controls.Add(this.labelNom);
            this.Controls.Add(this.labelVitesse);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelStyle);
            this.Controls.Add(this.textBoxFruit1);
            this.Controls.Add(this.textBoxFruit2);
            this.Controls.Add(this.textBoxFruit3);
            this.Controls.Add(this.textBoxViande1);
            this.Controls.Add(this.textBoxViande2);
            this.Controls.Add(this.textBoxViande3);
            this.Controls.Add(this.textBoxBoulangerie1);
            this.Controls.Add(this.textBoxNom);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Saisi";
            this.Text = "Saisie de recettes";
            this.TransparencyKey = System.Drawing.Color.DarkSlateBlue;
            this.Load += new System.EventHandler(this.Saisi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.TextBox textBoxBoulangerie1;
        private System.Windows.Forms.TextBox textBoxViande3;
        private System.Windows.Forms.TextBox textBoxViande2;
        private System.Windows.Forms.TextBox textBoxViande1;
        private System.Windows.Forms.TextBox textBoxFruit3;
        private System.Windows.Forms.TextBox textBoxFruit2;
        private System.Windows.Forms.TextBox textBoxFruit1;
        private System.Windows.Forms.Label labelStyle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelVitesse;
        private System.Windows.Forms.Label labelNom;
        private System.Windows.Forms.Label labelSousStyle;
        private System.Windows.Forms.ComboBox comboBoxStyle;
        private System.Windows.Forms.ComboBox comboBoxSousStyle;
        private System.Windows.Forms.ComboBox comboBoxVitesse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxBoulangerie2;
        private System.Windows.Forms.TextBox textBoxDejeuner1;
        private System.Windows.Forms.TextBox textBoxPate2;
        private System.Windows.Forms.TextBox textBoxDejeuner2;
        private System.Windows.Forms.TextBox textBoxPate1;
        private System.Windows.Forms.TextBox textBoxPate3;
        private System.Windows.Forms.TextBox textBoxCongelo1;
        private System.Windows.Forms.TextBox textBoxGateau4;
        private System.Windows.Forms.TextBox textBoxGateau3;
        private System.Windows.Forms.TextBox textBoxGateau2;
        private System.Windows.Forms.TextBox textBoxGateau1;
        private System.Windows.Forms.TextBox textBoxLaitier3;
        private System.Windows.Forms.TextBox textBoxLaitier2;
        private System.Windows.Forms.TextBox textBoxLaitier1;
        private System.Windows.Forms.TextBox textBoxCongelo2;
        private System.Windows.Forms.TextBox textBoxCongelo3;
        private System.Windows.Forms.Button buttonRecord;
        private System.Windows.Forms.Button buttonSearch;
    }
}