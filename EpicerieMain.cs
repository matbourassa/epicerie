﻿using System;
using System.Windows.Forms;

namespace Liste_Epiceri
{
    public partial class EpicerieMain : Form
    {
        DataViewer dataviewer = new DataViewer();
        Generateur generateur = new Generateur();
        Saisi saisi = new Saisi();

        public EpicerieMain()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
        }

        private void Epicerie_Load(object sender, EventArgs e)
        {         

        }
        
        
        private void saisiDeDonnéesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saisi.MdiParent = this;
            saisi.Dock = DockStyle.Fill;
            saisi.Show();
        }

        private void générateurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            generateur.MdiParent = this;
            generateur.Dock = DockStyle.Fill;
            generateur.Show();
        }

        private void baseDeDonnéesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataviewer.MdiParent = this;
            dataviewer.Dock = DockStyle.Fill;
            dataviewer.Show();

        }
        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void toolsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        
    }
}
