﻿namespace Liste_Epiceri
{
    partial class DataViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.repaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.styleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sousStyleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vitesseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientLegume1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientLegume2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientLegume3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientViande1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientViande2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientViande3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientBoulangerie1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientBoulangerie2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientDejeuner1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientDejeuner2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientLaitier1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientLaitier2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientLaitier3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientPate1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientPate2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientPate3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientGateau1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientGateau2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientGateau3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientGateau4DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientCongelo1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientCongelo2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingredientCongelo3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.styleDataGridViewTextBoxColumn,
            this.sousStyleDataGridViewTextBoxColumn,
            this.nomDataGridViewTextBoxColumn,
            this.vitesseDataGridViewTextBoxColumn,
            this.ingredientLegume1DataGridViewTextBoxColumn,
            this.ingredientLegume2DataGridViewTextBoxColumn,
            this.ingredientLegume3DataGridViewTextBoxColumn,
            this.ingredientViande1DataGridViewTextBoxColumn,
            this.ingredientViande2DataGridViewTextBoxColumn,
            this.ingredientViande3DataGridViewTextBoxColumn,
            this.ingredientBoulangerie1DataGridViewTextBoxColumn,
            this.ingredientBoulangerie2DataGridViewTextBoxColumn,
            this.ingredientDejeuner1DataGridViewTextBoxColumn,
            this.ingredientDejeuner2DataGridViewTextBoxColumn,
            this.ingredientLaitier1DataGridViewTextBoxColumn,
            this.ingredientLaitier2DataGridViewTextBoxColumn,
            this.ingredientLaitier3DataGridViewTextBoxColumn,
            this.ingredientPate1DataGridViewTextBoxColumn,
            this.ingredientPate2DataGridViewTextBoxColumn,
            this.ingredientPate3DataGridViewTextBoxColumn,
            this.ingredientGateau1DataGridViewTextBoxColumn,
            this.ingredientGateau2DataGridViewTextBoxColumn,
            this.ingredientGateau3DataGridViewTextBoxColumn,
            this.ingredientGateau4DataGridViewTextBoxColumn,
            this.ingredientCongelo1DataGridViewTextBoxColumn,
            this.ingredientCongelo2DataGridViewTextBoxColumn,
            this.ingredientCongelo3DataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.repaBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1885, 983);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // repaBindingSource
            // 
            this.repaBindingSource.DataSource = typeof(Liste_Epiceri.Repa);
            this.repaBindingSource.CurrentChanged += new System.EventHandler(this.repaBindingSource_CurrentChanged);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.Width = 43;
            // 
            // styleDataGridViewTextBoxColumn
            // 
            this.styleDataGridViewTextBoxColumn.DataPropertyName = "Style";
            this.styleDataGridViewTextBoxColumn.HeaderText = "Style";
            this.styleDataGridViewTextBoxColumn.Name = "styleDataGridViewTextBoxColumn";
            this.styleDataGridViewTextBoxColumn.Width = 55;
            // 
            // sousStyleDataGridViewTextBoxColumn
            // 
            this.sousStyleDataGridViewTextBoxColumn.DataPropertyName = "SousStyle";
            this.sousStyleDataGridViewTextBoxColumn.HeaderText = "SousStyle";
            this.sousStyleDataGridViewTextBoxColumn.Name = "sousStyleDataGridViewTextBoxColumn";
            this.sousStyleDataGridViewTextBoxColumn.Width = 79;
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "Nom";
            this.nomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            this.nomDataGridViewTextBoxColumn.Width = 54;
            // 
            // vitesseDataGridViewTextBoxColumn
            // 
            this.vitesseDataGridViewTextBoxColumn.DataPropertyName = "Vitesse";
            this.vitesseDataGridViewTextBoxColumn.HeaderText = "Vitesse";
            this.vitesseDataGridViewTextBoxColumn.Name = "vitesseDataGridViewTextBoxColumn";
            this.vitesseDataGridViewTextBoxColumn.Width = 66;
            // 
            // ingredientLegume1DataGridViewTextBoxColumn
            // 
            this.ingredientLegume1DataGridViewTextBoxColumn.DataPropertyName = "IngredientLegume1";
            this.ingredientLegume1DataGridViewTextBoxColumn.HeaderText = "IngredientLegume1";
            this.ingredientLegume1DataGridViewTextBoxColumn.Name = "ingredientLegume1DataGridViewTextBoxColumn";
            this.ingredientLegume1DataGridViewTextBoxColumn.Width = 123;
            // 
            // ingredientLegume2DataGridViewTextBoxColumn
            // 
            this.ingredientLegume2DataGridViewTextBoxColumn.DataPropertyName = "IngredientLegume2";
            this.ingredientLegume2DataGridViewTextBoxColumn.HeaderText = "IngredientLegume2";
            this.ingredientLegume2DataGridViewTextBoxColumn.Name = "ingredientLegume2DataGridViewTextBoxColumn";
            this.ingredientLegume2DataGridViewTextBoxColumn.Width = 123;
            // 
            // ingredientLegume3DataGridViewTextBoxColumn
            // 
            this.ingredientLegume3DataGridViewTextBoxColumn.DataPropertyName = "IngredientLegume3";
            this.ingredientLegume3DataGridViewTextBoxColumn.HeaderText = "IngredientLegume3";
            this.ingredientLegume3DataGridViewTextBoxColumn.Name = "ingredientLegume3DataGridViewTextBoxColumn";
            this.ingredientLegume3DataGridViewTextBoxColumn.Width = 123;
            // 
            // ingredientViande1DataGridViewTextBoxColumn
            // 
            this.ingredientViande1DataGridViewTextBoxColumn.DataPropertyName = "IngredientViande1";
            this.ingredientViande1DataGridViewTextBoxColumn.HeaderText = "IngredientViande1";
            this.ingredientViande1DataGridViewTextBoxColumn.Name = "ingredientViande1DataGridViewTextBoxColumn";
            this.ingredientViande1DataGridViewTextBoxColumn.Width = 118;
            // 
            // ingredientViande2DataGridViewTextBoxColumn
            // 
            this.ingredientViande2DataGridViewTextBoxColumn.DataPropertyName = "IngredientViande2";
            this.ingredientViande2DataGridViewTextBoxColumn.HeaderText = "IngredientViande2";
            this.ingredientViande2DataGridViewTextBoxColumn.Name = "ingredientViande2DataGridViewTextBoxColumn";
            this.ingredientViande2DataGridViewTextBoxColumn.Width = 118;
            // 
            // ingredientViande3DataGridViewTextBoxColumn
            // 
            this.ingredientViande3DataGridViewTextBoxColumn.DataPropertyName = "IngredientViande3";
            this.ingredientViande3DataGridViewTextBoxColumn.HeaderText = "IngredientViande3";
            this.ingredientViande3DataGridViewTextBoxColumn.Name = "ingredientViande3DataGridViewTextBoxColumn";
            this.ingredientViande3DataGridViewTextBoxColumn.Width = 118;
            // 
            // ingredientBoulangerie1DataGridViewTextBoxColumn
            // 
            this.ingredientBoulangerie1DataGridViewTextBoxColumn.DataPropertyName = "IngredientBoulangerie1";
            this.ingredientBoulangerie1DataGridViewTextBoxColumn.HeaderText = "IngredientBoulangerie1";
            this.ingredientBoulangerie1DataGridViewTextBoxColumn.Name = "ingredientBoulangerie1DataGridViewTextBoxColumn";
            this.ingredientBoulangerie1DataGridViewTextBoxColumn.Width = 141;
            // 
            // ingredientBoulangerie2DataGridViewTextBoxColumn
            // 
            this.ingredientBoulangerie2DataGridViewTextBoxColumn.DataPropertyName = "IngredientBoulangerie2";
            this.ingredientBoulangerie2DataGridViewTextBoxColumn.HeaderText = "IngredientBoulangerie2";
            this.ingredientBoulangerie2DataGridViewTextBoxColumn.Name = "ingredientBoulangerie2DataGridViewTextBoxColumn";
            this.ingredientBoulangerie2DataGridViewTextBoxColumn.Width = 141;
            // 
            // ingredientDejeuner1DataGridViewTextBoxColumn
            // 
            this.ingredientDejeuner1DataGridViewTextBoxColumn.DataPropertyName = "IngredientDejeuner1";
            this.ingredientDejeuner1DataGridViewTextBoxColumn.HeaderText = "IngredientDejeuner1";
            this.ingredientDejeuner1DataGridViewTextBoxColumn.Name = "ingredientDejeuner1DataGridViewTextBoxColumn";
            this.ingredientDejeuner1DataGridViewTextBoxColumn.Width = 128;
            // 
            // ingredientDejeuner2DataGridViewTextBoxColumn
            // 
            this.ingredientDejeuner2DataGridViewTextBoxColumn.DataPropertyName = "IngredientDejeuner2";
            this.ingredientDejeuner2DataGridViewTextBoxColumn.HeaderText = "IngredientDejeuner2";
            this.ingredientDejeuner2DataGridViewTextBoxColumn.Name = "ingredientDejeuner2DataGridViewTextBoxColumn";
            this.ingredientDejeuner2DataGridViewTextBoxColumn.Width = 128;
            // 
            // ingredientLaitier1DataGridViewTextBoxColumn
            // 
            this.ingredientLaitier1DataGridViewTextBoxColumn.DataPropertyName = "IngredientLaitier1";
            this.ingredientLaitier1DataGridViewTextBoxColumn.HeaderText = "IngredientLaitier1";
            this.ingredientLaitier1DataGridViewTextBoxColumn.Name = "ingredientLaitier1DataGridViewTextBoxColumn";
            this.ingredientLaitier1DataGridViewTextBoxColumn.Width = 113;
            // 
            // ingredientLaitier2DataGridViewTextBoxColumn
            // 
            this.ingredientLaitier2DataGridViewTextBoxColumn.DataPropertyName = "IngredientLaitier2";
            this.ingredientLaitier2DataGridViewTextBoxColumn.HeaderText = "IngredientLaitier2";
            this.ingredientLaitier2DataGridViewTextBoxColumn.Name = "ingredientLaitier2DataGridViewTextBoxColumn";
            this.ingredientLaitier2DataGridViewTextBoxColumn.Width = 113;
            // 
            // ingredientLaitier3DataGridViewTextBoxColumn
            // 
            this.ingredientLaitier3DataGridViewTextBoxColumn.DataPropertyName = "IngredientLaitier3";
            this.ingredientLaitier3DataGridViewTextBoxColumn.HeaderText = "IngredientLaitier3";
            this.ingredientLaitier3DataGridViewTextBoxColumn.Name = "ingredientLaitier3DataGridViewTextBoxColumn";
            this.ingredientLaitier3DataGridViewTextBoxColumn.Width = 113;
            // 
            // ingredientPate1DataGridViewTextBoxColumn
            // 
            this.ingredientPate1DataGridViewTextBoxColumn.DataPropertyName = "IngredientPate1";
            this.ingredientPate1DataGridViewTextBoxColumn.HeaderText = "IngredientPate1";
            this.ingredientPate1DataGridViewTextBoxColumn.Name = "ingredientPate1DataGridViewTextBoxColumn";
            this.ingredientPate1DataGridViewTextBoxColumn.Width = 107;
            // 
            // ingredientPate2DataGridViewTextBoxColumn
            // 
            this.ingredientPate2DataGridViewTextBoxColumn.DataPropertyName = "IngredientPate2";
            this.ingredientPate2DataGridViewTextBoxColumn.HeaderText = "IngredientPate2";
            this.ingredientPate2DataGridViewTextBoxColumn.Name = "ingredientPate2DataGridViewTextBoxColumn";
            this.ingredientPate2DataGridViewTextBoxColumn.Width = 107;
            // 
            // ingredientPate3DataGridViewTextBoxColumn
            // 
            this.ingredientPate3DataGridViewTextBoxColumn.DataPropertyName = "IngredientPate3";
            this.ingredientPate3DataGridViewTextBoxColumn.HeaderText = "IngredientPate3";
            this.ingredientPate3DataGridViewTextBoxColumn.Name = "ingredientPate3DataGridViewTextBoxColumn";
            this.ingredientPate3DataGridViewTextBoxColumn.Width = 107;
            // 
            // ingredientGateau1DataGridViewTextBoxColumn
            // 
            this.ingredientGateau1DataGridViewTextBoxColumn.DataPropertyName = "IngredientGateau1";
            this.ingredientGateau1DataGridViewTextBoxColumn.HeaderText = "IngredientGateau1";
            this.ingredientGateau1DataGridViewTextBoxColumn.Name = "ingredientGateau1DataGridViewTextBoxColumn";
            this.ingredientGateau1DataGridViewTextBoxColumn.Width = 120;
            // 
            // ingredientGateau2DataGridViewTextBoxColumn
            // 
            this.ingredientGateau2DataGridViewTextBoxColumn.DataPropertyName = "IngredientGateau2";
            this.ingredientGateau2DataGridViewTextBoxColumn.HeaderText = "IngredientGateau2";
            this.ingredientGateau2DataGridViewTextBoxColumn.Name = "ingredientGateau2DataGridViewTextBoxColumn";
            this.ingredientGateau2DataGridViewTextBoxColumn.Width = 120;
            // 
            // ingredientGateau3DataGridViewTextBoxColumn
            // 
            this.ingredientGateau3DataGridViewTextBoxColumn.DataPropertyName = "IngredientGateau3";
            this.ingredientGateau3DataGridViewTextBoxColumn.HeaderText = "IngredientGateau3";
            this.ingredientGateau3DataGridViewTextBoxColumn.Name = "ingredientGateau3DataGridViewTextBoxColumn";
            this.ingredientGateau3DataGridViewTextBoxColumn.Width = 120;
            // 
            // ingredientGateau4DataGridViewTextBoxColumn
            // 
            this.ingredientGateau4DataGridViewTextBoxColumn.DataPropertyName = "IngredientGateau4";
            this.ingredientGateau4DataGridViewTextBoxColumn.HeaderText = "IngredientGateau4";
            this.ingredientGateau4DataGridViewTextBoxColumn.Name = "ingredientGateau4DataGridViewTextBoxColumn";
            this.ingredientGateau4DataGridViewTextBoxColumn.Width = 120;
            // 
            // ingredientCongelo1DataGridViewTextBoxColumn
            // 
            this.ingredientCongelo1DataGridViewTextBoxColumn.DataPropertyName = "IngredientCongelo1";
            this.ingredientCongelo1DataGridViewTextBoxColumn.HeaderText = "IngredientCongelo1";
            this.ingredientCongelo1DataGridViewTextBoxColumn.Name = "ingredientCongelo1DataGridViewTextBoxColumn";
            this.ingredientCongelo1DataGridViewTextBoxColumn.Width = 124;
            // 
            // ingredientCongelo2DataGridViewTextBoxColumn
            // 
            this.ingredientCongelo2DataGridViewTextBoxColumn.DataPropertyName = "IngredientCongelo2";
            this.ingredientCongelo2DataGridViewTextBoxColumn.HeaderText = "IngredientCongelo2";
            this.ingredientCongelo2DataGridViewTextBoxColumn.Name = "ingredientCongelo2DataGridViewTextBoxColumn";
            this.ingredientCongelo2DataGridViewTextBoxColumn.Width = 124;
            // 
            // ingredientCongelo3DataGridViewTextBoxColumn
            // 
            this.ingredientCongelo3DataGridViewTextBoxColumn.DataPropertyName = "IngredientCongelo3";
            this.ingredientCongelo3DataGridViewTextBoxColumn.HeaderText = "IngredientCongelo3";
            this.ingredientCongelo3DataGridViewTextBoxColumn.Name = "ingredientCongelo3DataGridViewTextBoxColumn";
            this.ingredientCongelo3DataGridViewTextBoxColumn.Width = 124;
            // 
            // DataViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1804, 792);
            this.Controls.Add(this.dataGridView1);
            this.Name = "DataViewer";
            this.Text = "Base de données";
            this.Load += new System.EventHandler(this.DataViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource repaBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn styleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sousStyleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vitesseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientLegume1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientLegume2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientLegume3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientViande1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientViande2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientViande3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientBoulangerie1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientBoulangerie2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientDejeuner1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientDejeuner2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientLaitier1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientLaitier2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientLaitier3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientPate1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientPate2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientPate3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientGateau1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientGateau2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientGateau3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientGateau4DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientCongelo1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientCongelo2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ingredientCongelo3DataGridViewTextBoxColumn;
    }
}