﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;

using System.Reflection;
using Microsoft.Office.Core;
using Microsoft.Win32;
using System.Windows.Documents;
using System.Windows.Controls;
using System.Windows.Input;
using System.Diagnostics;

namespace Liste_Epiceri
{
    public partial class Generateur : Form
    {

        //lien pour se connecter sur le SQLServer
        string connectionString = @"Data Source=MATHIEU\BOUMSERVER;Initial Catalog=Epicerie;Integrated Security=True;
                                    Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        SqlConnection sqlCon = new SqlConnection();
        Dictionary<string, int> repasCurrentlyShown = new Dictionary<string, int>();  //prend le nom du jour et l<index    


        public Generateur()
        {
            InitializeComponent();
        }

        private void Generateur_Load(object sender, EventArgs e)
        {


        }



        private void labelJeudi_Click(object sender, EventArgs e)
        {

        }

        private void labelListe_Click(object sender, EventArgs e)
        {

        }


        private void buttonSouperVendredi_Click(object sender, EventArgs e)
        {
            sqlCon.ConnectionString = connectionString;
            sqlCon.Open();
            int index = indexRandomizer();
            string vitesseDePreparation = comboBoxSouperVendredi.Text;

            if (checkBoxVendredi.Checked == true)
            {
                repasCurrentlyShown.Remove("vendredi");
                grabRandomSouper(index, "vendredi", vitesseDePreparation);
                repasCurrentlyShown.Add("vendredi", index);


                if (string.IsNullOrEmpty(grabRandomSouper(index, "vendredi", vitesseDePreparation)))  //Si la string revient null, c est que ce n etait pas un souper, alors on reshuffle une fois de plus  
                {
                    sqlCon.Close();
                    buttonSouperVendredi_Click(sender, e);
                }
                else
                    sqlCon.Close();
            }
            else
            {
                repasCurrentlyShown.Remove("vendredi");
                grabRandomSouper(index, "vendredi", vitesseDePreparation);
                textBoxSouperVendredi.Clear();
                sqlCon.Close();
            }
        }

        private void buttonSouperSamedi_Click(object sender, EventArgs e)
        {
            sqlCon.ConnectionString = connectionString;
            sqlCon.Open();
            int index = indexRandomizer();
            string vitesseDePreparation = comboBoxSouperSamedi.Text;

            if (checkBoxSamedi.Checked == true)
            {
                repasCurrentlyShown.Remove("samedi");
                grabRandomSouper(index, "samedi", vitesseDePreparation);
                repasCurrentlyShown.Add("samedi", index);

                if (string.IsNullOrEmpty(grabRandomSouper(index, "samedi", vitesseDePreparation)))  //Si la string revient null, c est que ce n etait pas un souper, alors on reshuffle une fois de plus
                {
                    sqlCon.Close();
                    buttonSouperSamedi_Click(sender, e);
                }
                else
                    sqlCon.Close();
            }
            else
            {
                repasCurrentlyShown.Remove("samedi");
                grabRandomSouper(index, "samedi", vitesseDePreparation);
                textBoxSouperSamedi.Clear();
                sqlCon.Close();
            }
        }

        private void buttonSouperDimanche_Click(object sender, EventArgs e)
        {
            sqlCon.ConnectionString = connectionString;
            sqlCon.Open();
            int index = indexRandomizer();
            string vitesseDePreparation = comboBoxSouperDimanche.Text;

            if (checkBoxDimanche.Checked == true)
            {
                repasCurrentlyShown.Remove("dimanche");
                grabRandomSouper(index, "dimanche", vitesseDePreparation);
                repasCurrentlyShown.Add("dimanche", index);

                if (string.IsNullOrEmpty(grabRandomSouper(index, "dimanche", vitesseDePreparation)))  //Si la string revient null, c est que ce n etait pas un souper, alors on reshuffle une fois de plus
                {
                    sqlCon.Close();
                    buttonSouperDimanche_Click(sender, e);
                }
                else
                    sqlCon.Close();
            }
            else
            {
                repasCurrentlyShown.Remove("dimanche");
                grabRandomSouper(index, "dimanche", vitesseDePreparation);
                textBoxSouperDimanche.Clear();
                sqlCon.Close();
            }

        }

        private void buttonSouperLundi_Click(object sender, EventArgs e)
        {
            sqlCon.ConnectionString = connectionString;
            sqlCon.Open();
            int index = indexRandomizer();
            string vitesseDePreparation = comboBoxSouperLundi.Text;

            if (checkBoxLundi.Checked == true)
            {
                repasCurrentlyShown.Remove("lundi");
                grabRandomSouper(index, "lundi", vitesseDePreparation);
                repasCurrentlyShown.Add("lundi", index);

                if (string.IsNullOrEmpty(grabRandomSouper(index, "lundi", vitesseDePreparation)))  //Si la string revient null, c est que ce n etait pas un souper, alors on reshuffle une fois de plus
                {
                    sqlCon.Close();
                    buttonSouperLundi_Click(sender, e);
                }
                else
                    sqlCon.Close();
            }
            else
            {
                repasCurrentlyShown.Remove("lundi");
                grabRandomSouper(index, "lundi", vitesseDePreparation);
                textBoxSouperLundi.Clear();
                sqlCon.Close();

            }
        }

        private void buttonSouperMardi_Click(object sender, EventArgs e)
        {
            sqlCon.ConnectionString = connectionString;
            sqlCon.Open();
            int index = indexRandomizer();
            string vitesseDePreparation = comboBoxSouperMardi.Text;

            if (checkBoxMardi.Checked == true)
            {
                repasCurrentlyShown.Remove("mardi");
                grabRandomSouper(index, "mardi", vitesseDePreparation);
                repasCurrentlyShown.Add("mardi", index);

                if (string.IsNullOrEmpty(grabRandomSouper(index, "mardi", vitesseDePreparation)))  //Si la string revient null, c est que ce n etait pas un souper, alors on reshuffle une fois de plus
                {
                    sqlCon.Close();
                    buttonSouperMardi_Click(sender, e);
                }
                else
                    sqlCon.Close();
            }
            else
            {
                repasCurrentlyShown.Remove("mardi");
                grabRandomSouper(index, "mardi", vitesseDePreparation);
                textBoxSouperMardi.Clear();
                sqlCon.Close();
            }

        }

        private void buttonSouperMercredi_Click(object sender, EventArgs e)
        {
            sqlCon.ConnectionString = connectionString;
            sqlCon.Open();
            int index = indexRandomizer();
            string vitesseDePreparation = comboBoxSouperMercredi.Text;

            if (checkBoxMercredi.Checked == true)
            {
                repasCurrentlyShown.Remove("mercredi");
                grabRandomSouper(index, "mercredi", vitesseDePreparation);
                repasCurrentlyShown.Add("mercredi", index);

                if (string.IsNullOrEmpty(grabRandomSouper(index, "mercredi", vitesseDePreparation)))  //Si la string revient null, c est que ce n etait pas un souper, alors on reshuffle une fois de plus
                {
                    sqlCon.Close();
                    buttonSouperMercredi_Click(sender, e);
                }
                else
                    sqlCon.Close();
            }
            else
            {
                repasCurrentlyShown.Remove("mercredi");
                grabRandomSouper(index, "mercredi", vitesseDePreparation);
                textBoxSouperMercredi.Clear();
                sqlCon.Close();

            }

        }

        private void buttonSouperJeudi_Click(object sender, EventArgs e)
        {

            sqlCon.ConnectionString = connectionString;
            sqlCon.Open();
            int index = indexRandomizer();
            string vitesseDePreparation = comboBoxSouperJeudi.Text;

            if (checkBoxJeudi.Checked == true)
            {
                repasCurrentlyShown.Remove("jeudi");
                grabRandomSouper(index, "jeudi", vitesseDePreparation);
                repasCurrentlyShown.Add("jeudi", index);

                if (string.IsNullOrEmpty(grabRandomSouper(index, "jeudi", vitesseDePreparation)))  //Si la string (nom du repas) revient null, c est que ce n etait pas un souper, alors on reshuffle une fois de plus
                {
                    sqlCon.Close();
                    buttonSouperJeudi_Click(sender, e);
                }
                else
                    sqlCon.Close();
            }
            else
            {
                repasCurrentlyShown.Remove("jeudi");
                grabRandomSouper(index, "jeudi", vitesseDePreparation);
                textBoxSouperJeudi.Clear();
                sqlCon.Close();
            }

        }

        private void buttonDinerSamedi_Click(object sender, EventArgs e)
        {

            sqlCon.ConnectionString = connectionString;
            sqlCon.Open();
            int index = indexRandomizer();

            if (checkBoxDinerSamedi.Checked == true)
            {
                repasCurrentlyShown.Remove("dinersamedi");
                grabRandomDiner(index, "samedi");
                repasCurrentlyShown.Add("dinersamedi", index);

                if (string.IsNullOrEmpty(grabRandomDiner(index, "samedi")))  //Si la string (nom du repas) revient null, c est que ce n etait pas un diner, alors on reshuffle une fois de plus
                {
                    sqlCon.Close();
                    buttonDinerSamedi_Click(sender, e);
                }
                else
                    sqlCon.Close();
            }
            else
            {
                repasCurrentlyShown.Remove("dinersamedi");
                grabRandomDiner(index, "samedi");
                textBoxDinerSamedi.Clear();
                sqlCon.Close();
            }
        }

        private void buttonDinerDimanche_Click(object sender, EventArgs e)
        {
            sqlCon.ConnectionString = connectionString;
            sqlCon.Open();
            int index = indexRandomizer();

            if (checkBoxDinerDimanche.Checked == true)
            {
                repasCurrentlyShown.Remove("dinerdimanche");
                grabRandomDiner(index, "dimanche");
                repasCurrentlyShown.Add("dinerdimanche", index);

                if (string.IsNullOrEmpty(grabRandomDiner(index, "dimanche")))  //Si la string (nom du repas) revient null, c est que ce n etait pas un diner, alors on reshuffle une fois de plus
                {
                    sqlCon.Close();
                    buttonDinerDimanche_Click(sender, e);
                }
                else
                    sqlCon.Close();
            }
            else
            {
                repasCurrentlyShown.Remove("dinerdimanche");
                grabRandomDiner(index, "dimanche");
                textBoxDinerDimanche.Clear();
                sqlCon.Close();
            }
        }

        private void buttonSouperAll_Click(object sender, EventArgs e)
        {
            buttonSouperJeudi_Click(sender, e);
            buttonSouperVendredi_Click(sender, e);
            buttonSouperSamedi_Click(sender, e);
            buttonSouperDimanche_Click(sender, e);
            buttonSouperLundi_Click(sender, e);
            buttonSouperMardi_Click(sender, e);
            buttonSouperMercredi_Click(sender, e);
        }

        private void buttonDinerAll_Click(object sender, EventArgs e)
        {
            
            buttonDinerSamedi_Click(sender, e);
            buttonDinerDimanche_Click(sender, e);
        }

        private void buttonWeekAll_Click(object sender, EventArgs e)
        {
            buttonSouperAll_Click(sender, e);
            buttonDinerAll_Click(sender, e);
        }

        private string grabRandomDiner(int randomIndex, string day)
        {
            SqlCommand comDiner = new SqlCommand("select Nom from Repas where Style = 'Diner' and ID = '" + randomIndex + "' ", sqlCon);//on sort un des diners au hasard de la table
            string nomDuRepas = Convert.ToString(comDiner.ExecuteScalar());
            ClearList();

            foreach (KeyValuePair<string, int> item in repasCurrentlyShown)
            {
                writeIngredientToList(item.Value);
            }

            if (day == "samedi") textBoxDinerSamedi.Text = nomDuRepas;
            if (day == "dimanche") textBoxDinerDimanche.Text = nomDuRepas;

            return nomDuRepas;
        }

        protected string grabRandomSouper(int randomIndex, string day, string vitesse)
        {

            if (vitesse == "")   //n'importe quel souper toutes vitesses confondues
            {
                SqlCommand comSouper1 = new SqlCommand("select Nom from Repas where Style = 'Souper' and ID = '" + randomIndex + "' ", sqlCon);//on sort un des soupers au hasard de la table
                string nomDuRepas1 = Convert.ToString(comSouper1.ExecuteScalar());
                ClearList();

                foreach (KeyValuePair<string, int> item in repasCurrentlyShown)
                {
                    writeIngredientToList(item.Value);
                }

                if (day == "jeudi") textBoxSouperJeudi.Text = nomDuRepas1;
                if (day == "vendredi") textBoxSouperVendredi.Text = nomDuRepas1;
                if (day == "samedi") textBoxSouperSamedi.Text = nomDuRepas1;
                if (day == "dimanche") textBoxSouperDimanche.Text = nomDuRepas1;
                if (day == "lundi") textBoxSouperLundi.Text = nomDuRepas1;
                if (day == "mardi") textBoxSouperMardi.Text = nomDuRepas1;
                if (day == "mercredi") textBoxSouperMercredi.Text = nomDuRepas1;

                return nomDuRepas1;

            }
            else  //souper avec un choix de vitesse donné
            {
                SqlCommand comSouper2 = new SqlCommand("select Nom from Repas where Style = 'Souper' and ID = '" + randomIndex + "' and Vitesse = '" + vitesse + "' ", sqlCon);//on sort un des soupers au hasard de la table
                string nomDuRepas2 = Convert.ToString(comSouper2.ExecuteScalar());

                ClearList();

                foreach (KeyValuePair<string, int> item in repasCurrentlyShown)
                {
                    writeIngredientToList(item.Value);
                }

                if (day == "jeudi") textBoxSouperJeudi.Text = nomDuRepas2;
                if (day == "vendredi") textBoxSouperVendredi.Text = nomDuRepas2;
                if (day == "samedi") textBoxSouperSamedi.Text = nomDuRepas2;
                if (day == "dimanche") textBoxSouperDimanche.Text = nomDuRepas2;
                if (day == "lundi") textBoxSouperLundi.Text = nomDuRepas2;
                if (day == "mardi") textBoxSouperMardi.Text = nomDuRepas2;
                if (day == "mercredi") textBoxSouperMercredi.Text = nomDuRepas2;

                return nomDuRepas2;


            }

        }

        private int indexRandomizer()  //Retourne un int au hasard qui correspondra a un index dans la table 
        {
            Random rdn = new Random();

            SqlCommand comNumberOfRows = new SqlCommand("select count(*) from Repas", sqlCon);  //grab le nombre de row dans la table 
            int numberOfRow = Convert.ToInt32(comNumberOfRows.ExecuteScalar());                 //et l<enregistre sur <<numberOrRow>>, il s agit de la liste des index ID disponibles   

            int randomID = rdn.Next(1, numberOfRow + 1);    //On choisi au hasard un index dans la liste des ID - il n<y a pas d index 0

            return randomID;
        }


        private void ClearList()
        {
            textBoxListeFruits.Clear();
            textBoxListeViande.Clear();
            textBoxListeBoulangerie.Clear();
            textBoxListeDejeuner.Clear();
            textBoxListeLait.Clear();
            textBoxListePate.Clear();
            textBoxListeGateau.Clear();
            textBoxListeCongelo.Clear();

            
        }

        protected void writeIngredientToList(int index)
        {

            //ClearList();

            //commande Sql qui prend le nom des ingredients de l index selectionne
            SqlCommand comShowList = new SqlCommand("SELECT IngredientLegume1, IngredientLegume2, IngredientLegume3, IngredientViande1, IngredientViande2, IngredientViande3," +
                                                  "IngredientBoulangerie1, IngredientBoulangerie2, IngredientDejeuner1, IngredientDejeuner2,  IngredientLaitier1, IngredientLaitier2, IngredientLaitier3," +
                                                  "IngredientPate1, IngredientPate2, IngredientPate3,  IngredientGateau1, IngredientGateau2, IngredientGateau3, IngredientGateau4," +
                                                  "IngredientCongelo1, IngredientCongelo2, IngredientCongelo3 FROM Repas WHERE ID = '" + index + "'", sqlCon);

            //On ecrit l<info dans les textbox designe
            SqlDataReader readerIn = comShowList.ExecuteReader();

            
                while (readerIn.Read())
                {
                    textBoxListeFruits.Text += (readerIn.GetString(0).Trim() + "\n");
                    textBoxListeFruits.Text += (readerIn.GetString(1).Trim() + "\n");
                    textBoxListeFruits.Text += (readerIn.GetString(2).Trim() + "\n");

                    textBoxListeViande.Text += (readerIn.GetString(3).Trim() + "\n");
                    textBoxListeViande.Text += (readerIn.GetString(4).Trim() + "\n");
                    textBoxListeViande.Text += (readerIn.GetString(5).Trim() + "\n");

                    textBoxListeBoulangerie.Text += (readerIn.GetString(6).Trim() + "\n");
                    textBoxListeBoulangerie.Text += (readerIn.GetString(7).Trim() + "\n");

                    textBoxListeDejeuner.Text += (readerIn.GetString(8).Trim() + "\n");
                    textBoxListeDejeuner.Text += (readerIn.GetString(9).Trim() + "\n");

                    textBoxListeLait.Text += (readerIn.GetString(10).Trim() + "\n");
                    textBoxListeLait.Text += (readerIn.GetString(11).Trim() + "\n");
                    textBoxListeLait.Text += (readerIn.GetString(12).Trim() + "\n");

                    textBoxListePate.Text += (readerIn.GetString(13).Trim() + "\n");
                    textBoxListePate.Text += (readerIn.GetString(14).Trim() + "\n");
                    textBoxListePate.Text += (readerIn.GetString(15).Trim() + "\n");

                    textBoxListeGateau.Text += (readerIn.GetString(16).Trim() + "\n");
                    textBoxListeGateau.Text += (readerIn.GetString(17).Trim() + "\n");
                    textBoxListeGateau.Text += (readerIn.GetString(18).Trim() + "\n");
                    textBoxListeGateau.Text += (readerIn.GetString(19).Trim() + "\n");

                    textBoxListeCongelo.Text += (readerIn.GetString(20).Trim() + "\n");
                    textBoxListeCongelo.Text += (readerIn.GetString(21).Trim() + "\n");
                    textBoxListeCongelo.Text += (readerIn.GetString(22).Trim() + "\n");
                }
                readerIn.Close();
            
        }

        private void richTextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxListe_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }


        private void buttonOutExcel_Click(object sender, EventArgs e)
        {

            _Application excel = new Microsoft.Office.Interop.Excel.Application();  //new excel object
            _Workbook workBook = excel.Workbooks.Add(Type.Missing);
            _Worksheet workSheet = null;

            try
            {
                PageSetup(workSheet, workBook);
                PrintSouper(workSheet, workBook);
                PrintDiner(workSheet, workBook);

                PrintFruit(workSheet, workBook);
                PrintViande(workSheet, workBook);
                PrintBoulangerie(workSheet, workBook);
                PrintDejeuners(workSheet, workBook);
                PrintLaitier(workSheet, workBook);
                PrintPate(workSheet, workBook);
                PrintGateau(workSheet, workBook);
                PrintCongelo(workSheet, workBook);


                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    workBook.SaveAs(saveFileDialog1.FileName);
                    Process.Start("excel.exe", saveFileDialog1.FileName);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                excel.Quit();
                workBook = null;
                excel = null;
            }

        }

        private void PageSetup(_Worksheet workSheet, _Workbook workBook)
        {
            workSheet = workBook.ActiveSheet;

            for (int i = 1; i < 6; i++)  //setting up Columns width
            {
                workSheet.Columns[i].ColumnWidth = 17;
            }

            for (int i = 1; i < 43; i++)   //setting up rows height
            {
                workSheet.Rows[i].RowHeight = 16;
            }

            Range range = workSheet.Range["A1:E34"];  //selecting a section of the page
            range.Borders.Color = System.Drawing.Color.Black.ToArgb();  //drawing borders around cells in the range
            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter; //center on every cells in the range

            workSheet.Cells[1, 3].Font.Bold = true;
            workSheet.Cells[1, 3].Font.Underline = true;
            workSheet.Cells[1, 3] = "Fruits et Légumes";

            workSheet.Cells[5, 3].Font.Bold = true;
            workSheet.Cells[5, 3].Font.Underline = true;
            workSheet.Cells[5, 3] = "Viande";

            workSheet.Cells[9, 3].Font.Bold = true;
            workSheet.Cells[9, 3].Font.Underline = true;
            workSheet.Cells[9, 3] = "Boulangerie";

            workSheet.Cells[12, 3].Font.Bold = true;
            workSheet.Cells[12, 3].Font.Underline = true;
            workSheet.Cells[12, 3] = "Déjeuners";

            workSheet.Cells[15, 3].Font.Bold = true;
            workSheet.Cells[15, 3].Font.Underline = true;
            workSheet.Cells[15, 3] = "Produits laitiers";

            workSheet.Cells[19, 3].Font.Bold = true;
            workSheet.Cells[19, 3].Font.Underline = true;
            workSheet.Cells[19, 3] = "Riz et pâtes";

            workSheet.Cells[25, 3].Font.Bold = true;
            workSheet.Cells[25, 3].Font.Underline = true;
            workSheet.Cells[25, 3] = "Barres tendres/jus";

            workSheet.Cells[29, 3].Font.Bold = true;
            workSheet.Cells[29, 3].Font.Underline = true;
            workSheet.Cells[29, 3] = "Pharmacie";

            workSheet.Cells[32, 3].Font.Bold = true;
            workSheet.Cells[32, 3].Font.Underline = true;
            workSheet.Cells[32, 3] = "Boissons et congélateur";

            workSheet.Cells[35, 1].Font.Bold = true;
            workSheet.Cells[35, 1] = "Lundi";

            workSheet.Cells[36, 1].Font.Bold = true;
            workSheet.Cells[36, 1] = "Mardi";

            workSheet.Cells[37, 1].Font.Bold = true;
            workSheet.Cells[37, 1] = "Mercredi";

            workSheet.Cells[38, 1].Font.Bold = true;
            workSheet.Cells[38, 1] = "Jeudi";

            workSheet.Cells[39, 1].Font.Bold = true;
            workSheet.Cells[39, 1] = "Vendredi";

            workSheet.Cells[40, 1].Font.Bold = true;
            workSheet.Cells[40, 1] = "Samedi";

            workSheet.Cells[41, 1].Font.Bold = true;
            workSheet.Cells[41, 1] = "Dimanche";

            workSheet.Cells[43, 1].Font.Bold = true;
            workSheet.Cells[43, 1] = "Samedi midi";

            workSheet.Cells[44, 1].Font.Bold = true;
            workSheet.Cells[44, 1] = "Dimanche midi";

        }

        private void PrintDiner(_Worksheet workSheet, _Workbook workBook)
        {
            workSheet = workBook.ActiveSheet;

            workSheet.Cells[43, 2] = textBoxDinerSamedi.Text.Trim();
            workSheet.Cells[44, 2] = textBoxDinerDimanche.Text.Trim();
        }

        private void PrintSouper(_Worksheet workSheet, _Workbook workBook)
        {
            workSheet = workBook.ActiveSheet;

            workSheet.Cells[35, 2] = textBoxSouperLundi.Text.Trim();
            workSheet.Cells[36, 2] = textBoxSouperMardi.Text.Trim();
            workSheet.Cells[37, 2] = textBoxSouperMercredi.Text.Trim();
            workSheet.Cells[38, 2] = textBoxSouperJeudi.Text.Trim();
            workSheet.Cells[39, 2] = textBoxSouperVendredi.Text.Trim();
            workSheet.Cells[40, 2] = textBoxSouperSamedi.Text.Trim();
            workSheet.Cells[41, 2] = textBoxSouperDimanche.Text.Trim();
        }


        private void PrintFruit(_Worksheet workSheet, _Workbook workBook)
        {
            int row = 2;
            int column = 1;
            const int numberOfColunm = 5;
            string[] removeLine = new string[] { "\n" };
            string[] fruitFormated = textBoxListeFruits.Text.Split(removeLine, StringSplitOptions.RemoveEmptyEntries);

            workSheet = workBook.ActiveSheet;

            foreach (string list in fruitFormated)
            {
                workSheet.Cells[row, column] = list;
                column++;
                if (column > numberOfColunm)
                {
                    column = 1;
                    row++;
                }
            }
        }

        private void PrintViande(_Worksheet workSheet, _Workbook workBook)
        {
            int row = 6;
            int column = 1;
            const int numberOfColunm = 5;
            string[] removeLine = new string[] { "\n" };
            string[] viandeFormated = textBoxListeViande.Text.Split(removeLine, StringSplitOptions.RemoveEmptyEntries);


            workSheet = workBook.ActiveSheet;

            foreach (string list in viandeFormated)
            {
                workSheet.Cells[row, column] = list;
                column++;
                if (column > numberOfColunm)
                {
                    column = 1;
                    row++;
                }
            }
        }

        private void PrintBoulangerie(_Worksheet workSheet, _Workbook workBook)
        {
            int row = 10;
            int column = 1;
            const int numberOfColunm = 5;
            string[] removeLine = new string[] { "\n" };
            string[] viandeFormated = textBoxListeBoulangerie.Text.Split(removeLine, StringSplitOptions.RemoveEmptyEntries);


            workSheet = workBook.ActiveSheet;

            foreach (string list in viandeFormated)
            {
                workSheet.Cells[row, column] = list;
                column++;
                if (column > numberOfColunm)
                {
                    column = 1;
                    row++;
                }
            }
        }

        private void PrintDejeuners(_Worksheet workSheet, _Workbook workBook)
        {
            int row = 13;
            int column = 1;
            const int numberOfColunm = 5;
            string[] removeLine = new string[] { "\n" };
            string[] viandeFormated = textBoxListeDejeuner.Text.Split(removeLine, StringSplitOptions.RemoveEmptyEntries);


            workSheet = workBook.ActiveSheet;

            foreach (string list in viandeFormated)
            {
                workSheet.Cells[row, column] = list;
                column++;
                if (column > numberOfColunm)
                {
                    column = 1;
                    row++;
                }
            }
        }

        private void PrintLaitier(_Worksheet workSheet, _Workbook workBook)
        {
            int row = 16;
            int column = 1;
            const int numberOfColunm = 5;
            string[] removeLine = new string[] { "\n" };
            string[] viandeFormated = textBoxListeLait.Text.Split(removeLine, StringSplitOptions.RemoveEmptyEntries);


            workSheet = workBook.ActiveSheet;

            foreach (string list in viandeFormated)
            {
                workSheet.Cells[row, column] = list;
                column++;
                if (column > numberOfColunm)
                {
                    column = 1;
                    row++;
                }
            }
        }

        private void PrintPate(_Worksheet workSheet, _Workbook workBook)
        {
            int row = 20;
            int column = 1;
            const int numberOfColunm = 5;
            string[] removeLine = new string[] { "\n" };
            string[] viandeFormated = textBoxListePate.Text.Split(removeLine, StringSplitOptions.RemoveEmptyEntries);


            workSheet = workBook.ActiveSheet;

            foreach (string list in viandeFormated)
            {
                workSheet.Cells[row, column] = list;
                column++;
                if (column > numberOfColunm)
                {
                    column = 1;
                    row++;
                }
            }
        }

        private void PrintGateau(_Worksheet workSheet, _Workbook workBook)
        {
            int row = 26;
            int column = 1;
            const int numberOfColunm = 5;
            string[] removeLine = new string[] { "\n" };
            string[] viandeFormated = textBoxListeGateau.Text.Split(removeLine, StringSplitOptions.RemoveEmptyEntries);


            workSheet = workBook.ActiveSheet;

            foreach (string list in viandeFormated)
            {
                workSheet.Cells[row, column] = list;
                column++;
                if (column > numberOfColunm)
                {
                    column = 1;
                    row++;
                }
            }
        }

        private void PrintCongelo(_Worksheet workSheet, _Workbook workBook)
        {
            int row = 33;
            int column = 1;
            const int numberOfColunm = 5;
            string[] removeLine = new string[] { "\n" };
            string[] viandeFormated = textBoxListeCongelo.Text.Split(removeLine, StringSplitOptions.RemoveEmptyEntries);


            workSheet = workBook.ActiveSheet;

            foreach (string list in viandeFormated)
            {
                workSheet.Cells[row, column] = list;
                column++;
                if (column > numberOfColunm)
                {
                    column = 1;
                    row++;
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void SelectSouper_Click(object sender, EventArgs e)
        {
            checkBoxVendredi.Checked = true;
            checkBoxSamedi.Checked = true;
            checkBoxDimanche.Checked = true;
            checkBoxLundi.Checked = true;
            checkBoxMardi.Checked = true;
            checkBoxMercredi.Checked = true;
            checkBoxJeudi.Checked = true;
            
        }

        private void SelectDiner_Click(object sender, EventArgs e)
        {
            checkBoxDinerSamedi.Checked = true;
            checkBoxDinerDimanche.Checked = true;
        }
    }


}







