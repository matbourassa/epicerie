﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Liste_Epiceri
{
    public partial class DataViewer : Form
    {
        //lien pour se connecter sur le SQLServer
        string connectionString = @"Data Source=MATHIEU\BOUMSERVER;Initial Catalog=Epicerie;Integrated Security=True;
                                    Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        SqlDataAdapter dataAdapter;
        DataTable table;


        public DataViewer()
        {
            InitializeComponent();
        }

        private void DataViewer_Load(object sender, EventArgs e)
        {

            dataGridView1.DataSource = repaBindingSource;
            GetData("Select * from Repas");
        }

        private void GetData(string selectCommand)
        {
            try
            {
                dataAdapter = new SqlDataAdapter(selectCommand, connectionString);
                table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;  //ne sert a rien dans mon contexte
                dataAdapter.Fill(table);
                repaBindingSource.DataSource = table;

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }




        public void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void repaBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }


        
    }
}
