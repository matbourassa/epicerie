﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Liste_Epiceri
{
    public partial class Saisi : Form
    {
        //lien pour se connecter sur le SQLServer
        string connectionString = @"Data Source=MATHIEU\BOUMSERVER;Initial Catalog=Epicerie;Integrated Security=True;
                                    Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        SqlDataAdapter dataAdapter;
        DataTable table;


        public Saisi()
        {

            InitializeComponent();
        }

        private void Saisi_Load(object sender, EventArgs e)
        {

            GetData("Select * from Repas");
        }

        private void GetData(string selectCommand)
        {
            try
            {
                dataAdapter = new SqlDataAdapter(selectCommand, connectionString);
                table = new DataTable();
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;  //ne sert a rien dans mon contexte
                dataAdapter.Fill(table);

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void comboBoxStyle_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxSousStyle_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBoxNom_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelSousStyle_Click(object sender, EventArgs e)
        {

        }

        private void labelNom_Click(object sender, EventArgs e)
        {

        }

        private void labelVitesse_Click(object sender, EventArgs e)
        {

        }

        private void labelStyle_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxVitesse_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void textBoxFruit1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonRecord_Click(object sender, EventArgs e)
        {
            SqlCommand command;

            string insert = @"insert into Repas(Style, SousStyle, Nom, Vitesse, IngredientLegume1, IngredientLegume2, IngredientLegume3, IngredientViande1, IngredientViande2, IngredientViande3,
                                                IngredientBoulangerie1, IngredientBoulangerie2, IngredientDejeuner1, IngredientDejeuner2, IngredientLaitier1, IngredientLaitier2, IngredientLaitier3,
                                                IngredientPate1, IngredientPate2, IngredientPate3, IngredientGateau1, IngredientGateau2, IngredientGateau3, IngredientGateau4, IngredientCongelo1,
                                                IngredientCongelo2, IngredientCongelo3)

                            values(@Style, @SousStyle, @Nom, @Vitesse, @IngredientLegume1, @IngredientLegume2, @IngredientLegume3, @IngredientViande1, @IngredientViande2, @IngredientViande3,
                                                @IngredientBoulangerie1, @IngredientBoulangerie2, @IngredientDejeuner1, @IngredientDejeuner2, @IngredientLaitier1, @IngredientLaitier2, @IngredientLaitier3,
                                                @IngredientPate1, @IngredientPate2, @IngredientPate3, @IngredientGateau1, @IngredientGateau2, @IngredientGateau3, @IngredientGateau4, @IngredientCongelo1,
                                                @IngredientCongelo2, @IngredientCongelo3)";


            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                try
                {
                    connection.Open();
                    command = new SqlCommand(insert, connection);
                    command.Parameters.AddWithValue(@"Style", comboBoxStyle.SelectedItem.ToString());
                    command.Parameters.AddWithValue(@"SousStyle", comboBoxSousStyle.SelectedItem.ToString());
                    command.Parameters.AddWithValue(@"Nom", textBoxNom.Text);
                    command.Parameters.AddWithValue(@"Vitesse", comboBoxVitesse.SelectedItem.ToString());
                    command.Parameters.AddWithValue(@"IngredientLegume1", textBoxFruit1.Text);
                    command.Parameters.AddWithValue(@"IngredientLegume2", textBoxFruit2.Text);
                    command.Parameters.AddWithValue(@"IngredientLegume3", textBoxFruit3.Text);
                    command.Parameters.AddWithValue(@"IngredientViande1", textBoxViande1.Text);
                    command.Parameters.AddWithValue(@"IngredientViande2", textBoxViande2.Text);
                    command.Parameters.AddWithValue(@"IngredientViande3", textBoxViande3.Text);
                    command.Parameters.AddWithValue(@"IngredientBoulangerie1", textBoxBoulangerie1.Text);
                    command.Parameters.AddWithValue(@"IngredientBoulangerie2", textBoxBoulangerie2.Text);
                    command.Parameters.AddWithValue(@"IngredientDejeuner1", textBoxDejeuner1.Text);
                    command.Parameters.AddWithValue(@"IngredientDejeuner2", textBoxDejeuner2.Text);
                    command.Parameters.AddWithValue(@"IngredientLaitier1", textBoxLaitier1.Text);
                    command.Parameters.AddWithValue(@"IngredientLaitier2", textBoxLaitier2.Text);
                    command.Parameters.AddWithValue(@"IngredientLaitier3", textBoxLaitier3.Text);
                    command.Parameters.AddWithValue(@"IngredientPate1", textBoxPate1.Text);
                    command.Parameters.AddWithValue(@"IngredientPate2", textBoxPate2.Text);
                    command.Parameters.AddWithValue(@"IngredientPate3", textBoxPate3.Text);
                    command.Parameters.AddWithValue(@"IngredientGateau1", textBoxGateau1.Text);
                    command.Parameters.AddWithValue(@"IngredientGateau2", textBoxGateau2.Text);
                    command.Parameters.AddWithValue(@"IngredientGateau3", textBoxGateau3.Text);
                    command.Parameters.AddWithValue(@"IngredientGateau4", textBoxGateau4.Text);
                    command.Parameters.AddWithValue(@"IngredientCongelo1", textBoxCongelo1.Text);
                    command.Parameters.AddWithValue(@"IngredientCongelo2", textBoxCongelo2.Text);
                    command.Parameters.AddWithValue(@"IngredientCongelo3", textBoxCongelo3.Text);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                textBoxNom.Clear();
                comboBoxSousStyle.SelectedItem = "";
                textBoxFruit1.Clear();
                textBoxFruit2.Clear();
                textBoxFruit3.Clear();
                textBoxViande1.Clear();
                textBoxViande2.Clear();
                textBoxViande3.Clear();
                textBoxBoulangerie1.Clear();
                textBoxBoulangerie2.Clear();
                textBoxDejeuner1.Clear();
                textBoxDejeuner2.Clear();
                textBoxLaitier1.Clear();
                textBoxLaitier2.Clear();
                textBoxLaitier3.Clear();
                textBoxPate1.Clear();
                textBoxPate2.Clear();
                textBoxPate3.Clear();
                textBoxGateau1.Clear();
                textBoxGateau2.Clear();
                textBoxGateau3.Clear();
                textBoxGateau3.Clear();
                textBoxGateau4.Clear();
                textBoxCongelo1.Clear();
                textBoxCongelo2.Clear();
                textBoxCongelo3.Clear();

            }





        }
    }
}
